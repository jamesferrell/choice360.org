<?php if(!defined('KIRBY')) exit ?>

username: mceraso
firstname: Melissa
lastname: Ceraso
email: mceraso@ala-choice.org
password: >
  $2a$10$ANCN2f1YlhPVr9SBNzB3cuSMKICjvPcnmdWx1rbJ5/5gBF0/9N8xa
language: en
role: editor
