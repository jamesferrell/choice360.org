<?php if(!defined('KIRBY')) exit ?>

username: berman
email: jferrell@bermanadvertising.com
password: >
  $2a$10$eqptflUeNxiI4d9zSJmz9OtWp0Xb8L3trYeHBqguy0dr7a8BIKOva
language: en
role: admin
history:
  - blog/ozark-folksong-collection
  - blog/ala-conference-2016
  - contact
  - products/reviews
  - products/magazine
