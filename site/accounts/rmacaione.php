<?php if(!defined('KIRBY')) exit ?>

username: rmacaione
firstname: Robert
lastname: Macaione
email: rmacaione@ala-choice.org
password: >
  $2a$10$.ps.d4rLrfUIOzlrp17gSegTRqDLhxYlQeGtobxXygsIa0NZgMj/S
language: en
role: admin
