<?php if(!defined('KIRBY')) exit ?>

username: bmickey
firstname: Bill
lastname: Mickey
email: bmickey@ala-choice.org
password: >
  $2a$10$TDBxPnEa2mLsCHku1BOtWuEKTy0G.7TeUVG0tiMihYJ2QR4qHMUV2
language: en
role: admin
