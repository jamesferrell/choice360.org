<?php if(!defined('KIRBY')) exit ?>

username: jstoehr
firstname: John
lastname: Stoehr
email: jstoehr@ala-choice.org
password: >
  $2a$10$T3OngF.XDO7bkLNFRcFXOOfM3nsfVrMMsb7PJcbE5qT0mg5GSvbw6
language: en
role: editor
