<?php if(!defined('KIRBY')) exit ?>

username: lmullen
firstname: Laura
lastname: Mullen
email: lmullen@ala-choice.org
password: >
  $2a$10$OPN.p8c6Ykh6H0CO1tjXH.wNA1DxNFsrNgOVUlwLKgUR9C5E2TkLW
language: en
role: admin
