<?

snippet('global.head');
snippet('global.menu');

snippet('home.header');

snippet('global.section', array(
  'field' => 'intro',
  'alignment' => 'u-left--center',
  'layout' => 'g-10 g-center scale--lg'
));

snippet('home.reviews');
snippet('global.section', array(
  'field' => 'librarianship',
  'alignment' => 'u-left--center',
  'layout' => 'g-10 g-center scale--lg'
));
snippet('home.magazine');
snippet('home.ccadvisor');
snippet('home.rcl');
snippet('home.blog');

snippet('global.cta');

snippet('global.footer');
