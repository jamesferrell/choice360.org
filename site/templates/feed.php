<?

  $blogs = $pages->find('blog')->children()->visible()->sortBy('date')->limit(10);
  $libs  = $pages->find('librarianship')->grandChildren()->visible()->sortBy('date')->limit(10);

  $items = new Pages(array($blogs, $libs));
  $items = $items->sortBy('date')->limit(10);

  snippet('feed', array(
    'link'  => url(''),
    'items' => $items,
    'descriptionField'  => 'description',
    'descriptionLength' => 150
  ));
