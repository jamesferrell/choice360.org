Title: Critical Library Pedagogy

----

Headline: 

----

Text: Critical pedagogy incorporates inclusive and reflective teaching for aims of social justice; it provides mechanisms for students to evaluate their social, political, and economic standing, and to question societal norms and how these norms perpetuate societal injustices. In two volumes, the Critical Library Pedagogy Handbook works to make critical pedagogy more accessible for library educators, examining both theory and practice to help the busy practitioner explore various aspects of teaching for social justice.

----

Cta: 