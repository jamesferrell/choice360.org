Title: Subscribe

----

Headline: Subscribe to Choice Reviews

----

Text:

## How to subscribe

1. Complete both the (file: croaccountform.pdf text: account activation form) and the (file: CROuse.pdf text: terms of use form).

2. Submit both forms to (email: support@acrlchoice.freshdesk.com).

We will activate your online subscription within forty-eight hours of receiving both completed forms.

----

Pricing:

## Pricing

<h3 class="u-no_margin delta">Subscription Rates, 2016-2017 Academic Year</h3>

The following rates apply to all new subscriptions with a start date of September 1, 2016, or later, and to all orders, new or renewal, placed after September 1, 2016, regardless of start date.

All rates are for a single site. For multisite locations, please (link: contact text: inquire about our discount policy).

(table…)

#### Academic Library (United States)
////
$599

++++

#### Academic Library (Non-US, including Canada and Mexico)
////
$689

++++

#### School Library (K-12)
////
$375

++++

#### Govt. Library (including USIA, etc.)
////
$600

++++

#### Public Library (United States)
////
$530

++++

#### Public Library (Non-US)
////
$579

++++

#### Other Library
////
$630

++++

#### Publishers/Dealers
////
$630

(…table)

----

CTA:

## Questions about subscribing? Just ask.

(link: contact text: Contact us class: button)
