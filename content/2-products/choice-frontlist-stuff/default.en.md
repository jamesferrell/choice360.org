Title: Choice Frontlist Stuff

----

Headline: 

----

Text: 

### Buy  some books, yo: <br> </br>

(image: critical-library-pedagogy-vol-1_cover.jpg link:http://www.alastore.ala.org/detail.aspx?ID=11883 alt: "critical library pedagogy vol 1" caption: Critical Pedagogy Volume 1 ) <br> </br> 

(image: critical-library-pedagogy-vol-2_cover.jpg alt: critical library pedagogy vol 2 caption: Critical Pedagogy Volume 2 link:http://www.alastore.ala.org/detail.aspx?ID=11883)<br> </br>

(image: bridging-worlds.jpg alt: briding worlds  caption: Bridging Worlds link:http://www.alastore.ala.org/detail.aspx?ID=11860)<br> </br>   

(image: collaborating-for-impact.jpg alt: collaborating for impact caption: Collaborating for Impact link:https://www.amazon.com/BC-Classics-Asadora-Barbecue-Portable/dp/B000SVX85A/ref=sr_1_3?ie=UTF8&qid=1477421666&sr=8-3&keywords=la+caja+china )<br> </br>

(image: databrarianship.jpg  alt: databrarianship caption: Databrarianship link: http://www.alastore.ala.org/detail.aspx?ID=11774)<br> </br> 

(image: rewired.jpg  alt: rewired caption: Rewired link:http://www.mysteryranch.com/3-day-assault-pack )<br> </br>

(image: the-discovery-tool-cookbook.jpg alt: discovery tool cookbook caption: Discovery Tool Cookbook link: http://www.alastore.ala.org/detail.aspx?ID=11758<br> </br>) 

(image: the-small-and-rural-academic-library.jpg alt: the small and rural academic library  caption: The Small and Rural Academic Library link: http://www.alastore.ala.org/detail.aspx?ID=11835)

----

Cta: 

## Take a look at some of ACRL’s recent offerings.

(link: http://www.ala.org/acrl/publications/booksanddigitalresources/booksmonographs/catalog/publications text: Go class: button)