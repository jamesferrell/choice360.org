Title: Subscribe

----

Headline: Subscribe to <em>Choice</em> magazine

----

Text: 

## How to subscribe

To subscribe, please contact our subscription agent at (email: ACRLSubscriptions@pubservice.com).

----

Pricing: 

## Pricing

<h3 class="u-no_margin delta">Subscription Rates, 2016–2017 Volume 54</h3>

The following rates apply to all subscriptions and renewals with a start date of September 1, 2016, or later, and to all orders placed after September 1, 2016, regardless of start date.

(table...)

#### Publication

(...column...)

#### Includes

(...column...)

#### United States

(...column...)

#### Canada & Mexico

(...column...)

#### Other International

(...row...)

**Choice Magazine**

(...column...)

12 monthly Issues

(...column...)

$469

(...column...)

$499

(...column...)

$599

(...row...)

**Choice Select**

(...column...)

Choice's 4 most popular issues:
- November (Reference)
-  January (OAT List)
-  May (University Press Issue)
-  August (Internet Issue)

(...column...)

$189

(...column...)

--

(...column...)

--

(...row...)

**Choice Reviews on Cards** (link: products/reviews/subscribe text: Reviews) or Magazine subscription required

(...column...)

12 monthly issues of Choice reviews in card format
(4.125" x 5.25" on 70 lb. paper)

(...column...)

$549

(...column...)

$589

(...column...)

$679

(...table)

----

Cta: 

## Questions about subscribing? Just ask.

(link: contact text: Contact us class: button)