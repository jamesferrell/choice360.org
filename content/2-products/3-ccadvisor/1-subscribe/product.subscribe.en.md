Title: Subscribe

----

Headline: Subscribe to CC Advisor

----

Text:

## How to subscribe

Complete both the Account Activation form and the Terms of Use form.  Submit both forms to (email: support@acrlchoice.freshdesk.com).

We will activate your online subscription within forty-eight hours of receiving both completed forms

----

Pricing:

## Pricing

<h3 class="u-no_margin delta">Subscription Rates, 2016-2017 Academic Year</h3>

The following rates apply to all new subscriptions. All rates are for a single site. For multisite locations, please (link: contact text: inquire about our discount policy).

Special discounts are available to subscribers to The Charleston Advisor. Please inquire about these at (link: contact).

(table…)

#### Academic Library (United States and Canada)
////
$349

++++

#### Academic Library (Excluding United States and Canada)
////
$599

++++

#### School Library (K-12)
////
$195

++++

#### Public Library
////
$349

++++

#### Publishers/Dealers
////
$749

(…table)

----

CTA:

## Questions about subscribing? Just ask.

(link: contact text: Contact us class: button)
