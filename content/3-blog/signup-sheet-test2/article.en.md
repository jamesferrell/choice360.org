Title: signup sheet test2

----

Subtitle: 

----

Category: 

----

Date: 

----

Description: 

----

Line1: 

----

Thumb: 

----

Hero: 

----

Hero-caption: 

----

Text: 

<!--Begin CTCT Sign-Up Form-->
<!-- EFD 1.0.0 [Tue Oct 11 14:09:08 EDT 2016] -->
<link rel='stylesheet' type='text/css' href='https://static.ctctcdn.com/h/contacts-embedded-signup-assets/1.0.2/css/signup-form.css'>
<link href="jQueryAssets/jquery.ui.core.min.css" rel="stylesheet" type="text/css">
<link href="jQueryAssets/jquery.ui.theme.min.css" rel="stylesheet" type="text/css">
<link href="jQueryAssets/jquery.ui.slider.min.css" rel="stylesheet" type="text/css">
<script src="jQueryAssets/jquery-1.11.1.min.js" type="text/javascript"></script>
<script src="jQueryAssets/jquery.ui-1.10.4.slider.min.js" type="text/javascript"></script>
<div class="ctct-embed-signup" style="font: 16px Helvetica Neue, Arial, sans-serif; font: 1rem Helvetica Neue, Arial, sans-serif; line-height: 1.5; -webkit-font-smoothing: antialiased;">
   <div style="color:#5b5b5b; background-color:#DEE1F1; border-radius:5px;">
       <span id="success_message" style="display:none;">
           <div style="text-align:center;">Thanks for signing up!</div>
       </span>
       <form data-id="embedded_signup:form" class="ctct-custom-form Form" name="embedded_signup" method="POST" action="https://visitor2.constantcontact.com/api/signup">
           <h2 style="margin:0;">Newsletter Sign Up Form</h2>
           <p>Thank you for your interest in joining the Choice Reviews mailing list. We look forward to keeping you informed.</p>
           <!-- The following code must be included to ensure your sign-up form works properly. -->
           <input data-id="ca:input" name="ca" value="5f500526-8a75-4d80-8e4f-d6ca166df31a" type="hidden">
           <input data-id="source:input" name="source" value="EFD" type="hidden">
           <input data-id="required:input" name="required" value="list,cf_text--1,email,first_name,last_name,job_title" type="hidden">
           <input data-id="url:input" name="url" value="" type="hidden">
           <p data-id="Email Address:p" ><label data-id="Email Address:label" data-name="email" class="ctct-form-required">Email Address</label> <input data-id="Email Address:input" name="email" value="" maxlength="80" type="text"></p>
           <p data-id="First Name:p" ><label data-id="First Name:label" data-name="first_name" class="ctct-form-required">First Name</label> <input data-id="First Name:input" name="first_name" value="" maxlength="50" type="text"></p>
           <p data-id="Last Name:p" ><label data-id="Last Name:label" data-name="last_name" class="ctct-form-required">Last Name</label> <input data-id="Last Name:input" name="last_name" value="" maxlength="50" type="text"></p>
           <div class="mc-field-group size1of2">
             <label for="mce-MMERGE6-month">Untitled </label>
           </div>
         <div class="mc-field-group">
           <label for="mce-MMERGE7">Untitled </label>
           <select name="MMERGE7" class="" id="mce-MMERGE7">
             <option value=""></option>
             <option value="First Choice">First Choice</option>
             <option value="Second Choice">Second Choice</option>
             <option value="Third Choice">Third Choice</option>
           </select>
           </div>
         <div class="mc-field-group">
             <label for="mce-MMERGE8">Untitled </label>
             <select name="MMERGE8" class="" id="mce-MMERGE8">
               <option value=""></option>
               <option value="First Choice">First Choice</option>
               <option value="Second Choice">Second Choice</option>
               <option value="Third Choice">Third Choice</option>
             </select>
           </div>
           <p data-id="Country:p" >
             <label data-id="Lists:label" data-name="list" class="ctct-form-required">Email Lists</label>
           </p>
           <div><input data-id="Lists:input" name="list_0" value="1692036450" type="checkbox"><span data-id="Lists:span">Community College Resources</span></div><div><input data-id="Lists:input" name="list_1" value="3" type="checkbox"><span data-id="Lists:span">Editors' Picks</span></div><div><input data-id="Lists:input" name="list_2" value="2" type="checkbox"><span data-id="Lists:span">Forthcoming Titles</span></div><div><input data-id="Lists:input" name="list_3" value="4" type="checkbox"><span data-id="Lists:span">Hot Topics</span></div><div><input data-id="Lists:input" name="list_4" value="9" type="checkbox"><span data-id="Lists:span">Internet Resources</span></div><div><input data-id="Lists:input" name="list_5" value="16" type="checkbox"><span data-id="Lists:span">Webinars</span></div></p>
           <button type="submit" class="Button ctct-button Button--block Button-secondary" data-enabled="enabled">Sign Up</button>
       	<div><p class="ctct-form-footer">By submitting this form, you are granting: CHOICE, 575 Main Street, Middletown, Connecticut, 06457, United States, http://www.ala.org/acrl/choice permission to email you. You may unsubscribe via the link found at the bottom of every email.  (See our <a href="http://www.constantcontact.com/legal/privacy-statement" target="_blank">Email Privacy Policy</a> for details.) Emails are serviced by Constant Contact.</p></div>
       </form>
   </div>
</div>
<script type='text/javascript'>
   var localizedErrMap = {};
   localizedErrMap['required'] = 		'This field is required.';
   localizedErrMap['ca'] = 			'An unexpected error occurred while attempting to send email.';
   localizedErrMap['email'] = 			'Please enter your email address in name@email.com format.';
   localizedErrMap['birthday'] = 		'Please enter birthday in MM/DD format.';
   localizedErrMap['anniversary'] = 	'Please enter anniversary in MM/DD/YYYY format.';
   localizedErrMap['custom_date'] = 	'Please enter this date in MM/DD/YYYY format.';
   localizedErrMap['list'] = 			'Please select at least one email list.';
   localizedErrMap['generic'] = 		'This field is invalid.';
   localizedErrMap['shared'] = 		'Sorry, we could not complete your sign-up. Please contact us to resolve this.';
   localizedErrMap['state_mismatch'] = 'Mismatched State/Province and Country.';
	localizedErrMap['state_province'] = 'Select a state/province';
   localizedErrMap['selectcountry'] = 	'Select a country';
   var postURL = 'https://visitor2.constantcontact.com/api/signup?ef07af6e-5966-4743-be8d-96f0af599de8';
</script>
<script type='text/javascript' src='https://static.ctctcdn.com/h/contacts-embedded-signup-assets/1.0.2/js/signup-form.js'></script>
<!--End CTCT Sign-Up Form-->
<script type="text/javascript">
$(function() {
	$( "#Slider1" ).slider(); 
});
</script>

----

Line2: 

----

Author: 

----

Author-img: 

----

Byline: 

----

References: 