Title: Internet Resources: February Edition

----

Subtitle: Selected reviews of digital reference resources from the February issue of Choice.

----

Category: Newsletters

----

Date: 2017-02-02

----

Description: Selected reviews of digital reference resources from the February issue of Choice.

----

Line1: 

----

Thumb: feb-ir-2017.jpg

----

Hero: feb-ir-2017.jpg

----

Hero-caption: 

----

Text: 

**Commonwealth Club.** 
(link: http://www.commonwealthclub.org/ text: http://www.commonwealthclub.org/ popup: yes)

[Revisited Nov'16] According to its banner, the *Commonwealth Club* is the "Leading national forum open to all for the impartial discussion of public issues important to the membership, community and nation." To this end, the club (founded in 1903) hosts panel discussions and interviews on a wide range of issues of local and national significance. Events include, for example, "Paradise Burning" (on California wildfires), "Autonomous Vehicles and the Future of Transport," discussion of the 2016 presidential race with Robert Reich, "True or False? A Field Guide to Lies in the Information Age," "Above the Law? Addressing Sexual Assault on Campus and Beyond," a weekly election roundtable, a series of forums on "The Future of Work," Steve Forbes on the economy, and sessions with celebrity guests (NBA star Kareem Abdul-Jabbar, Food Network host Ayesha Curry, actor-activist Alan Cumming, etc.) or politicians (e.g., Condoleezza Rice, Hillary Clinton).

The site contains forums and other material for members, but the chief attraction for nonmembers is the video archive. Videos are housed on *YouTube*, giving viewers the option of watching clips or entire shows. The site contains podcasts (also available through *iTunes* and* Google Play*), transcripts of events, mobile apps, press releases, and a blog. Sample searches on a range of subjects yielded results that include a mix of videos, transcripts, blog entries, and podcasts. Featured programs (Climate One, Good Lit, Food Lit, and Inforum) are found under the Events section. Visitors who like to browse can go directly to Watch & Listen, where menu options include Video, Audio, Radio Schedule, Broadcast Stations, Images, Buy a CD, and Commonwealth Club TV; video content dates back to 2009, although most is more recent. A detailed subject menu would improve the site's organization for easier browsing of its remarkably rich, informative content. **Summing Up:** Highly recommended. All readership levels. *—S. Clerc, Southern Connecticut State University*

****

**FRASER: Federal Reserve Archival System for Economic Research**, Federal Reserve Bank of St. Louis. 
(link: http://fraser.stlouisfed.org/ text: https://fraser.stlouisfed.org/ popup: yes)

[Revisited Nov'16] Established in 2004 by the Federal Reserve Bank of St. Louis ((link: http://www.choicereviews.org text: CH, Dec'05, 43-1935 popup: yes)), FRASER provides digital access to economic history through policy documents, press coverage, and economic and banking data. Digital material is added from, among other sources, the Library of Congress's broad digital program American Memory ((link: http://www.choicereviews.org text: CH, Dec'05, 43-2404 popup: yes)) and the Internet Archive ((link: http://www.choicereviews.org text: CH, Jun'13, 50-5327 popup: yes)), providing historical context to the Federal Reserve materials. A search box accesses all site resources, while the logo allows easy return to the homepage. A navigation bar provides access to economic data, Federal Reserve history, archival collections, and education, with classroom materials developed by Federal Reserve education specialists. The toolbar has browsing by author, title, and date. The last item on the toolbar points to the Theme section, which refers to collections of important documents relating to a range of topics. Below the toolbar are Explore (exploring themes), Access (economic data), Learn (Federal Reserve history), Inside FRASER (recently added topics and major resources), What's New (with a handful of recently added items), Popular Themes, Popular Items, @FedFRASER on *Twitter*, and a useful Need Help section.

FRASER data are supplemented by other Federal Reserve Bank of St. Louis sites, including FRED ((link:www.choicereviews.org http:// text: CH, Jan'14, 51-2766 popup: yes)) at (link: http://fred.stlouisfed.org/ text: https://fred.stlouisfed.org/ popup: yes), that supply data sets and tools to download, graph, and track more than 400,000 US and international time series from nearly 80 sources, while the archival economic data discoverable at *ALFRED* (*Archival FRED*) at (link: http://alfred.stlouisfed.org/ text: https://alfred.stlouisfed.org/ popup: yes) preserves older versions of economic data available from specific dates. *Federal Reserve History* at (link: http://www.federalreservehistory.org/ text: http://www.federalreservehistory.org/ popup: yes) provides topical essays with bibliographies on the people, events, and purposes of the Federal Reserve. Educators will find a range of print and online education resources for economics and personal finance at (link: http://www.stlouisfed.org/education/ text: https://www.stlouisfed.org/education/ popup: yes). *FRASER* and these several companion sites are valuable resources for history and economics research. **Summing Up: **Recommended. All libraries. All levels. *—P. A. Frisch, Our Lady of the Lake University*

***

**International Bibliography of Humanism and the Renaissance**. Brepols. Annual academic subscription begins at $545.00, based on FTE and Carnegie Classification. 
(link: http://www.brepols.net/ text: http://www.brepols.net/ popup: yes)

[Visited Nov'16] In concentrating on 16th- and 17th-century European history and culture, European exploration, and European influence throughout the world, the *International Bibliography of Humanism and the Renaissance* (*IBHR*) indexes a variety of scholarly publications, including monographs, articles, chapters, encyclopedias, theses, exhibition catalogues, and more. Also indexed are materials related to the interpretation and teaching of texts from the Renaissance and early modern eras. Compiled originally by Librairie Droz starting in 1965 as the *Bibliographie Internationale de l'Humanisme et de la Renaissance*, the resource was acquired by Brepols only in 2013. To make it fully cross-searchable using the same interface as other Brepols databases, such as the International Medieval Bibliography ((link: http://www.choicereviews.org text: CH, Sep'12, 50-0004 popup: yes)) or the Bibliography of British and Irish History Online ((link: http://www.choicereviews.org text: CH, Jan'11, 48-2439 popup: yes)), the editors are in the process of adjusting the indexing and thesauri as needed. Presently, over 333,000 records are included, and an international team of contributors adds 20,000 new titles annually.

The basic interface allows users to search by general keyword, author, century, "discipline tree" (an expandable list of broad fields of study, including art history, economics, literature, science, women's studies, and more), and geographic area (from Africa to southeastern Europe, displayed in a pop-up window like the discipline tree). The advanced search offers these options plus field searching by title, journal or series, publication type, publication year, or century; the help files explain that pre-2010 publications are not yet indexed by century. Typing in the search boxes activates an autocomplete function (showing spelling variations and abbreviated elements), and the interface reveals the number of hits before a search is performed—two very helpful features.

*IBHR* is a multilingual database, with interfaces offered in English, French, German, Spanish, and Italian. Resources from all languages and formats are included, but one must locate materials written in a particular language by doing a general keyword search. Search results can be exported into popular bibliographic management programs, including EndNote ((link: http://www.choicereviews.org text: CH, Feb'08, 45-2929 popup: yes)), Zotero ((link: http://www.choicereviews.org text: CH, Jun'08, 45-5309 popup: yes)), *RefWorks*, or Microsoft Word, and researchers can set up email alerts for subjects of interest. The database is open-URL compliant, and retrieved records easily link to the external full-text resources available through an institution's licensed online journals or Google Books ((link: http://www.choicereviews.org text: CH, Dec'12, 50-1800 popup: yes)), for example. The contents could be amplified in some areas—there are comparatively few theses indexed, for example—but *IBHR* is a work in progress, with the editors making a concerted effort to add new and retrospective content. Overall, this versatile, content-rich, interdisciplinary source offers good international coverage that complements not only Brepols's other database offerings but also Iter: Gateway to the Middle Ages and Renaissance ((link: http://www.choicereviews.org text: CH, Feb'17, 54-2549 popup: yes)), the University of Toronto's bibliographic tool, which is comparable but has a broader historical scope. **Summing Up:** Recommended. Undergraduates through researchers/faculty; general readers. *—S. L. Johnson, Eastern Illinois University*

***

**Iter: Gateway to the Middle Ages and Renaissance**. University of Toronto Libraries. Annual academic subscription ranges from $280.00 to $1,125.00 based on FTE. 
(link: http://www.itergateway.org/ text: http://www.itergateway.org/ popup: yes)

[Visited Nov'16] This bibliographic database, whose name in Latin means "journey or path," indexes studies on medieval and early modern history and culture—the period spanning 400–1700. Produced since 1994 by a consortium of universities and societies and centered at the University of Toronto, its 1.3 million indexed resources include journal articles, edited volumes, monographs, book reviews, dissertation abstracts, and more. Journal coverage is quite deep (nearly 2,000 titles), although in some instances core journals are not covered in their entirety nor analyzed quickly; indexing at the time of this review of *Renaissance Quarterly*, for example, covers volume 20, 1967, to the beginning of 2015, whereas the indexing is complete and more current in the field's comparable resource, published by Brepols, the International Bibliography of Humanism and the Renaissance ((link: http://www.choicereviews.org text: CH, Feb'17, 54-2547 popup: yes)). Significant collections of essays have been recently added to provide historic depth back to 1874.

The interface features both basic and advanced searches—the latter allowing multifield (title, author, subject, and call number) searching, as well as an unstructured Boolean option for locating terms within the same field. However, not all records carry LC subject headings, and individual essays in books and journal articles are notably without subject entries. Results can be usefully filtered by various categories, including publication type, language, author, and publishing year, while some filter categories—subject time period and geographical area—will become more meaningful when these metadata are applied consistently to more records. The interface allows for easy marking, emailing, and checking for full text, and bibliographic management is enabled for Zotero ((link: http://www.choicereviews.org text: CH, Jun'08, 45-5309 popup: yes)).

The database had a large-scale annual update in October 2015 that added 20,000 records, and smaller updates occur monthly. Iter is one of a number of resources—bibliographies, journals, and ebooks—accessible on the Itergateway platform, most of which require a subscription. For students interested in Renaissance and early modern Europe, Iter offers wide coverage of the scholarship. But for the Middle Ages (particularly the centuries prior to the 1300s), material is far richer in Brepols' companion International Medieval Database ((link: http://www.choicereviews.org text: CH, Sep'12, 50-0004 popup: yes)). **Summing Up:** Recommended. Undergraduates through researchers/faculty; general readers. *—M. C. Schaus, Haverford College*

***

**Oceanus Magazine**. Woods Hole Oceanographic Institution.
(link: http://www.whoi.edu/oceanus/ text: http://www.whoi.edu/oceanus/ popup: yes)

[Visited Nov'16]  Oceanus Magazine is a glossy print magazine that is also available in an appealing, freely accessible, electronic format. Indexed by ProQuest and EBSCO and several other publishers of popular periodical indexes, including GREENR (Global Reference on the Environment, Energy, and Natural Resources) ((link: http://www.choicereviews.org text: CH, Jan'10, 47-2353 popup: yes)). The goal of the magazine is to highlight and explain the ongoing research in ocean exploration performed by the Woods Hole Oceanic Institute. The editors successfully accomplish this mission with a publication that is highly informative and profusely illustrated. The home page of the online magazine is well laid out and clearly highlights the latest articles. Additional stories can also be located through hyperlinks for broad topics, special issues, and multimedia. Hyperlinks are also available for featured videos, photographs, and interactive articles. Readers who prefer the more traditional layout can access a digital page-image copy of the magazine. Access to the archives of both the print and digital issues is available, and all segments of the website load easily and quickly. Subscribers can also sign up for email notifications to learn about new articles. The publication, reasonably priced in its print edition, is written at a level that is accessible to both the layperson and the scholar. **Summing Up:** Highly recommended. All readership levels. *—J. C. Stachacz, Wilkes University*

****

**Opera**. Exact Editions. Annual academic subscription begins at $375.00. 
(link: http://institutions.exacteditions.com/opera text: https://institutions.exacteditions.com/opera popup: yes)

[Visited Nov'16] A subscription to the online edition of *Opera* magazine provides fully searchable access to the contents of every issue—beginning with the first, published in 1950, through each new monthly issue. The site provides IP-authenticated remote web access, with no requirement to log in with username and password, and access is seamless, crossing iOS and Android platforms. As is well known to regular readers, the content of *Opera*, published in London, is similar to that of its American counterpart, the Metropolitan Opera's *Opera News* at (link: http://www.operanews.com/ text: http://www.operanews.com/ popup: yes), whose online archive, composed of issues dating back to 1949, is also accessible to subscribers. Both *Opera* and *Opera News* contain articles dealing with operatic repertoire, singers, conductors, and other personnel, as well as reviews of performances, important books and recordings, and performance schedules of opera companies worldwide. Although articles in both periodicals are geared to performers and opera enthusiasts rather than scholars (and keyword search finds matches in the ads as well as the features), many of those in *Opera* are written with the connoisseur in mind, while those in *Opera News* tend to be more popular in tone. Furthermore, the international coverage is more extensive in *Opera*, whereas the online version of *Opera News* provides access to videotapes, a feature lacking in the online *Opera* magazine. Conservatories and colleges with strong music programs will benefit from subscriptions to both digital periodicals. **Summing Up:** Highly recommended. Undergraduates through researchers/faculty; general readers; professionals/practitioners. *—D. Ossenkop, SUNY College at Potsdam*

***

**The Pulitzer Prizes**.
(link: http://www.pulitzer.org text: www.pulitzer.org/ popup: yes)

[Revisited Nov'16] Established in 1917, the Pulitzer Prize is recognized as one of the most prestigious awards in journalism, literature, and musical composition, awarding prizes and fellowships annually in 21 fields. *The Pulitzer Prizes* website is the place to learn more about the history of the awards and their creator, Joseph Pulitzer, a Hungarian émigré. It is also a tool for submitting work for consideration by the awards committees. Since the site was first reviewed nearly two decades ago ((link: http://www.choicereviews.org text: CH, Mar'98, 35-3626 popup: yes)), it has been expanded to offer original articles written for the site about the prizes and their histories, along with details about the finalists and winners. Users will find the sleek, attractive design easy to navigate and its basic search tool easy to use.

The site includes a complete listing of all winners from the first awards in 1917 (for editorial writing, the *New York Tribune* on the sinking of the Lusitania one year earlier) through the most recent winners of 2016 (for investigative reporting, breaking news reporting, feature writing, commentary, fiction, drama, poetry, music, and a range of other categories that casual observers might not be aware of), reflecting 100 years of US history. When the award-winning work itself is available online, the site provides a link. Winners may be sorted by year or category for easy browsing. Also included are news releases from the Pulitzer Prize Board, links to articles appearing in the mainstream press, a listing of upcoming events sponsored by the organization, and a separate section on the year-long centennial celebration with links to events, lectures, and exhibits. The layout and graphics are eye-catching, the pages load quickly, and the site functions equally well on computers and mobile devices. Like its namesake award, *The Pulitzer Prizes* website is tops in its field. **Summing Up:** Highly recommended. All libraries. All levels. *—C. A. Collins, Saint Joseph's University*

***

**The Women's Wear Daily Archive**. ProQuest. Contact publisher for pricing (based on FTE, purchase history, and other factors); annual academic subscription for 15,000 FTE begins at about $2,387.00; perpetual-access licensing also available.
(link: http://www.proquest.com/products-services/www.html text: http://www.proquest.com/products-services/www.html popup: yes)

[Visited Nov'16] The digital archive to *Women's Wear Daily*—originally *Women's Wear*, now simply *WWD*, and forever America's "rag trade rag"—has been a long time coming. From its inception, this publication has dutifully reported on both the big money and the big fluff that is the fashion industry. In the first issue of June 1, 1910, this huffy editorial protest from the *New York Times* was reprinted: "This spring's hats are the worst in shape and decoration that have ever disfigured the landscape, shocked artistic taste, and transformed tolerably good looking women into monstrosities." Hmm, nothing's changed it seems. Starting as a newspaper reporting on the openings, closings, and mergers of fashion companies, it covered the latest Parisian fashions, industry personnel changes, workplace actions, raw materials shortages, and news about fashionable socialites. It also ran advertisements. Now, more than 106 years later, the content is the same, only now it's bigger, full color, and often supplemented with special issues on fashion's auxiliaries: cosmetics, fragrance, and accessories. The archive allows one to browse or search articles and advertisements by decade, year, month, and day. As a trade newspaper, "daily" means Monday to Friday, and any particular day can have multiple issues.

Search screens are cleanly designed. Basic Search is simple: one rectangular box. Advanced Search supports command line searching, combining keywords with Boolean operators, and field searching by abstract, title, author, company/product, and document types (e.g., advertisements, obituaries) or features (e.g., illustrations, photographs, diagrams). A search for "baby boom" without quotes yielded more than a 1,000 hits, including bath products and booms in sales. Using double quotes yielded about half that number but also many irrelevant hits, reporting on baby fashion, not necessarily on the tens of millions in their mid-fifties and up. For that, "baby boomer" produced a more focused set and pluralized, even more (the assumption that ProQuest's interface can read one's mind is incorrect). Still, the strategy scored a terrific article: "The Neglected Market: Famous Boomers: Boomer Nostalgia: Boomer Advertising Boomers: $21 Billion," with a further banner in the PDF facsimile: "Ignore Them at Your Peril."

A bar graph allows filtering items by decade, revealing that "baby boomer" truncated is the focus of more than 700 articles from the 2000s, plummeting during 2010–16; the great recession perhaps? Or maybe time to pass the torch to the millennials, whose numbers began to supplant the boomers in the current decade. With this database, libraries can ditch their tatty paper *WWD* issues after about one year. Librarians who want to work effectively with fashion students should read this magazine regularly to develop a feel for its gossipy signature sections, like "Eye." The archive is an essential, easy-to-use, engrossing resource for all fashion collections. **Summing Up:** Essential. All readership levels. *—C. Donaldson, North Seattle College*

----

Line2: 

----

Author: Choice Staff

----

Author-img: 

----

Byline: 

----

References: 