Title: The Top 75 Community College Titles: May Edition

----

Subtitle: The best of all the titles appropriate for two-year colleges reviewed in the May issue of Choice.

----

Category: Newsletters

----

Date: 2017-05-30

----

Description: The best of all the titles appropriate for two-year colleges reviewed in the May issue of Choice.

----

Line1: 

----

Thumb: may-cc-2017.jpg

----

Hero: may-cc-2017.jpg

----

Hero-caption: 

----

Text: 

### REFERENCE
**Archives of Sexuality & Gender: LGBTQ History and Culture Since 1940**.  Gale, part of Cengage Learning. Contact publisher for pricing. URL:(link: http://www.gale.com/archives-of-sexuality-and-gender-lgbtq/ text: http://www.gale.com/archives-of-sexuality-and-gender-lgbtq/ popup: yes) May’17, 54-4067

**Crime and punishment in America: an encyclopedia of trends and controversies in the justice system**, ed. by Laura L. Finley. ABC-CLIO, 2017. 2v bibl index ISBN 9781610699273, $189.00; ISBN 9781610699280 ebook, contact publisher for price; 54-4069

Dowley, Tim. **Atlas of Christian history**. Fortress, 2016. 176p bibl index afp ISBN 9781451499704 pbk, $24.00; ISBN 9781506416885 ebook, contact publisher for price; 54-4052

**Melton’s encyclopedia of American religions: v.1: United States; v.2: Canada**, ed. by J. Gordon Melton with James Beverley, Con-stance Jones, and Pamela S. Nadell. 9th ed. Gale, part of Cengage Learning, 2016. 2v bibl index afp ISBN 9781414406879, $528.00; ISBN 9781414462653 ebook, contact publisher for price; 54-4056

**People of color in the United States: contemporary issues in education, work, communities, health, and immigration**, ed. by Kofi Lomotey. Greenwood, 2016. 4v bibl index afp ISBN 9781610698542, $399.00; ISBN 9781610698559 ebook, contact publisher for price; 54-4073

**The SAGE encyclopedia of contemporary early childhood education**, ed. by Donna Couchenour and J. Kent Chrisman. SAGE Publishing, 2016. 3v bibl index ISBN 9781483340357, $525.00; ISBN 9781483340333 ebook, contact publisher for price; 54-4074

Sheridan, Matthew J. **Exploring and understanding careers in criminal justice: a comprehensive guide**, by Matthew J. Sheridan and Raymond R. Rainville. Rowman & Littlefield, 2016. 296p bibl indexes afp ISBN 9781442254305, $38.00; ISBN 9781442254312 ebook, $37.99; 54-4075

**Understanding U.S. military conflicts through primary sources: v.1: American Revolution to Mexican-American War; v.2: American Civil War to Spanish-American War; v.3: World War I to Korean War; v.4: Vietnam War to Iraqi War**, ed. by James R. Arnold and Roberta Wiener. ABC-CLIO, 2015. 4v bibl index afp ISBN 9781610699334, $415.00; ISBN 9781610699341 ebook, contact publisher for price; 54-4077

Vile, John R. **The Jacksonian and antebellum eras: documents decoded**. ABC-CLIO, 2016. 279p bibl index ISBN 9781440849817, $81.00; ISBN 9781440849824 ebook, contact publisher for price; 54-4078

### HUMANITIES
**17th-century Chinese paintings from the Tsao family collection**, ed. by Stephen Little; with contributions by Raoul Birnbaum et al. Los Angeles County Museum of Art, 2016. 667p bibl index ISBN 9783791355214, $95.00; 54-4094

Aquila, Richard. **Let’s rock!: how 1950s America created Elvis and the rock and roll craze**.  Routledge, 2017. 351p bibl index ISBN 9781442269361, $40.00; ISBN 9781442269378 ebook, $39.99; 54-4154

Behlil, Melis. **Hollywood is everywhere: global directors in the blockbuster era**. Amsterdam University Press, 2016. 202p bibl in-dexes ISBN 9789089647399, $99.00; ISBN 9789048524976 ebook, contact publisher for price; 54-4147

Briley, Ron. **The ambivalent legacy of Elia Kazan: the politics of the post-HUAC films**. Rowman & Littlefield, 2017. 241p bibl index ISBN 9781442271678, $38.00; ISBN 9781442271685 ebook, $37.99; 54-4149

**The Cambridge companion to literature and religion**, [ed.] by Susan M. Felch. Cambridge, 2016. 289p bibl index ISBN 9781107097841, $89.99; ISBN 9781107483910 pbk, $29.99; ISBN 9781316160954 ebook, contact publisher for price; 54-4081

Chen, Xiaomei. **Staging Chinese revolution: theater, film, and the afterlives of propaganda**. Columbia, 2017. 363p bibl index ISBN 9780231166386, $60.00; ISBN 9780231541619 ebook, $59.99; 54-4083

Cox, Gary. **Existentialism and excess: the life and times of Jean-Paul Sartre**.  Bloomsbury Academic, 2016. 338p bibl index ISBN 9781474235334, $29.95; ISBN 9781474235358 ebook, contact publisher for price; 54-4168

De Conick, April D. **The Gnostic new age: how a countercultural spirituality revolutionized religion from antiquity to today**. Co-lumbia, 2016. 380p bibl index afp ISBN 9780231170765, $35.00; ISBN 9780231542043 ebook, $34.99; 54-4182

Gavaler, Chris. **On the origin of superheroes: from the big bang to Action comics no. 1**. Iowa, 2015. 295p bibl index afp ISBN 9781609383817 pbk, $18.00; ISBN 9781609383824 ebook, $18.00; 54-4108

Ibell, Paul. **Tennessee Williams**. Reaktion Books, 2016. 192p bibl ISBN 9781780236629 pbk, $19.00; ISBN 9781780237107 ebook, contact publisher for price; 54-4130

Kittelson, James M. **Luther the reformer: the story of the man and his career**, by James M. Kittelson and Hans H. Wiersma. 2nd ed. Fortress, 2016. 275p bibl indexes afp ISBN 9781451488883 pbk, $27.00; ISBN 9781506416861 ebook, $27.00; 54-4189

Rorty, Richard. **Philosophy as poetry**.  Virginia, 2016. 79p index ISBN 9780813939339, $19.95; ISBN 9780813939346 ebook, $19.95; 54-4176
Ruse, Michael. Darwinism as religion: what literature tells us about evolution.  Oxford, 2016. 310p bibl index ISBN 9780190241025, $34.95; ISBN 9780190241032 ebook, contact publisher for price; 54-4085

Schoenberg, Arnold. **Schoenberg’s models for beginners in composition**, ed. by Gordon Root. Oxford, 2016. 231p bibl index (Schoenberg in words, 2) ISBN 9780195382211, $99.00; ISBN 9780199700318 pbk, contact publisher for price; ISBN 9780199700318 ebook, contact publisher for price; 54-4161

Sherrell, Kat. **Experiencing Broadway music: a listener’s companion**. Rowman & Littlefield, 2016. 235p bibl index ISBN 9780810889002, $45.00; ISBN 9780810889019 ebook, $44.99; 54-4162

Soto, Michael. **Measuring the Harlem Renaissance: the U.S. census, African American identity, and literary form**. Massachusetts, 2016. 200p index ISBN 9781625342492, $90.00; ISBN 9781625342508 pbk, $24.95; 54-4139

### SCIENCE & TECHNOLOGY
Abdel-Motaal, Doaa. **Antarctica: the battle for the seventh continent**.  Praeger, 2016. 320p bibl index afp ISBN 9781440848032, $60.00; ISBN 9781440848049 ebook, contact publisher for price; 54-4240

Alpaydin, Ethem. **Machine learning: the new AI**. MIT, 2016. 206p bibl index afp ISBN 9780262529518 pbk, $15.95; ISBN 9780262337595 ebook, $11.95; 54-4268

**An Introduction to interdisciplinary research: theory and practice**, by Lucas Ruttingÿet al; ed. by Steph Menken and Machiel Keestra. Amsterdam University Press, 2016. 130p bibl (Perspectives on Interdisciplinarity, 2) ISBN 9789462981843 pbk, $31.50; ISBN 9789048531615 ebook, contact publisher for price; 54-4199

Bess, Michael. **Our grandchildren redesigned: life in the bioengineered society of the near future**. Beacon Press, 2015. 298p index afp ISBN 9780807052174, $28.95; ISBN 9780807052181 ebook, $20.00; 54-4259

**Big data is not a monolith**, ed. by Cassidy R. Sugimoto, Hamid R. Ekbia, and Michael Mattioli. MIT, 2016. 284p bibl index ISBN 9780262035057, $60.00; ISBN 9780262529488 pbk, $30.00; ISBN 9780262335775 ebook, $21.00; 54-4270

**Boxing Cuba: from backyards to world championship**, ed. by Michael Schleicher; tr. by Bram Opstelten; photographs by Kathari-na Alt. Hirmer, 2016. 182p ISBN 9783777426129, $42.00; 54-4295

Carolan, Michael S. **Society and the environment: pragmatic solutions to ecological issues**.  Westview, 2016. 340p bibl index ISBN 9780813350004 pbk, $68.00; ISBN 9780813350455 ebook, $43.99; 54-4244

Dech, Stefan. **Mountains: mapping the earth’s extremes**, by Stefan Dech, Reinhold Messner, and Nils Sparwasser. Thames & Hud-son/Deutsches Zentrum für Luft-und Raumfahrt, 2016. 241p index ISBN 9780500518892, $55.00; ISBN 9781402088766 ebook, contact publisher for price; 54-4246

Estupinyà, Pere. **S=EX²: the science of sex**, tr. by Mara Faye Lethem. Copernicus Books, 2016. 370p bibl afp ISBN 9783319317250 pbk, $19.99; ISBN 9783319317267 ebook, contact publisher for price; 54-4214

Hogan, Andrew J.** Life histories of genetic disease: patterns and prevention in postwar medical genetics**. Johns Hopkins, 2016. 259p index ISBN 9781421420745, $40.00; ISBN 9781421420752 ebook, $40.00; 54-4265

Langtangen, Hans Petter. **A primer on scientific programming with Python**. Springer, 2016. 914p index afp (Texts in computation-al science and engineering, 6) ISBN 9783662498866, $79.99; ISBN 9783662498873 ebook, $59.99; 54-4274

Miller, Ben. **The aliens are coming!: the extraordinary science behind our search for life in the universe**. The Experiment, 2016. 293p bibl ISBN 9781615193653 pbk, $15.95; ISBN 9781615193660 ebook, contact publisher for price; 54-4208

Mitchell, Lincoln Abraham. **Will big league baseball survive?: globalization, the end of television, youth sports, and the future of Major League Baseball**. Temple, 2017. 210p index afp ISBN 9781439913789, $94.50; ISBN 9781439913796 pbk, $24.95; ISBN 9781439913802 ebook, $24.95; 54-4300

Paul, Gregory S. **The Princeton field guide to dinosaurs**. Princeton, 2016. 360p bibl index afp ISBN 9780691167664, $35.00; ISBN 9781400883141 ebook, contact publisher for price; 54-4249

Quintiere, James G. **Principles of fire behavior**. CRC Press, 2017. 413p index ISBN 9781498735629, $101.96; 54-4256

Rhodes, Frank Harold Trevor. **Origins: the search for our prehistoric past**. Comstock Publishing Associates, 2016. 326p bibl index afp ISBN 9781501702440, $29.95; ISBN 9781501706233 ebook, contact publisher for price; 54-4222

**The Routledge handbook of philosophy of information**, ed. by Luciano Floridi. Routledge, 2016. 429p bibl index ISBN 9781138796935, $240.00; ISBN 9781315757544 ebook, contact publisher for price; 54-4277

Salkind, Neil J. **Statistics for people who (think they) hate statistics.** SAGE Publishing, 2017. 517p index afp ISBN 9781506333830 pbk, $82.00; ISBN 9781506333823 ebook, contact publisher for price; 54-4288

Simpson, Kevin E. **Soccer under the swastika: stories of survival and resistance during the Holocaust**. Rowman & Littlefield, 2016. 315p bibl index afp ISBN 9781442261624, $40.00; ISBN 9781442261631 ebook, $39.99; 54-4302

**Species and speciation in the fossil record**, ed. by Warren D. Allmon and Margaret M. Yacobucci. Chicago, 2016. 427p bibl index afp ISBN 9780226377445, $65.00; ISBN 9780226377582, $65.00; 54-4252

Stewart, Ian. **Calculating the cosmos: how mathematics unveils the universe**. Basic Books, 2016. 346p index ISBN 9780465096107, $27.99; ISBN 9780465096114 ebook, contact publisher for price; 54-4210

Vogel, Steven. **Why the wheel is round: muscles, technology, and how we make things move**.  Chicago, 2016. 327p bibl afp ISBN 9780226381039, $35.00; ISBN 9780226381176 ebook, $21.00; 54-4225

### SOCIAL &  BEHAVIORAL SCIENCES
Bachner, Jennifer. **What Washington gets wrong: the unelected officials who actually run the government and their misconcep-tions about the American people**, by Jennifer Bachner and Benjamin Ginsberg. Prometheus Books, 2016. 304p index ISBN 9781633882492, $25.00; ISBN 9781633882508 ebook, $11.99; 54-4479

Beckles, Hilary. **The first black slave society: Britain’s “barbarity time” in Barbados, 1636–1876**. University of the West Indies Press, 2016. 296p bibl index ISBN 9789766405854 pbk, $35.00; ISBN 9789766405878 ebook, contact publisher for price; 54-4367

Bellecourt, Clyde H. **The thunder before the storm: the autobiography of Clyde Bellecourt as told to Jon Lurie**, by Clyde H. Bellecourt and Jon Lurie. Minnesota Historical Society, 2016. 336p index ISBN 9781681340197, $27.95; ISBN 9781681340203 ebook, contact publisher for price; 54-4383

Blankenship, Anne M. **Christianity, social justice, and the Japanese American incarceration during World War II**. North Caroli-na, 2016. 282p index ISBN 9781469629193, $85.00; ISBN 9781469629209 pbk, $29.95; ISBN 9781469629216 ebook, $24.99; 54-4384

Davis, Thomas J. **History of African Americans: exploring diverse roots**. Greenwood, 2016. 271p bibl index ISBN 9780313385407, $58.00; ISBN 9780313385414 ebook, contact publisher for price; 54-4390

Dunar, Andrew J. **America in the Teens**. Syracuse, 2016. 318p bibl index afp ISBN 9780815634805, $65.00; ISBN 9780815634652 pbk, $29.95; ISBN 9780815653776 ebook, contact publisher for price; 54-4392

**Feminist futures: re-imagining women, culture and development**, ed. by Kum-Kum Bhavnany et al. 2nd ed. Zed Books, 2016. 499p bibl index ISBN 9781783606399, $95.00; ISBN 9781783606382 pbk, $29.95; ISBN 9781783606412 ebook, contact publish-er for price; 54-4512

Fritze, Ronald H. **Egyptomania: a history of fascination, obsession and fantasy**. Reaktion Books, 2016. 444p bibl index ISBN 9781780236391, $35.00; 54-4375

Geake, Robert A. **From slaves to soldiers: The 1st Rhode Island Regiment in the American Revolution**, by Robert A. Geake with Lorén M. Spears. Westholme, 2016. 171p bibl index ISBN 9781594162688, $26.00; 54-4396

Harper, Sarah. **How population change will transform our world**. Oxford, 2016. 234p index ISBN 9780198784098, $24.95; ISBN 9780191086687 ebook, contact publisher for price; 54-4308

Hinsch, Bret. **Women in imperial China**. Rowman & Littlefield, 2016. 255p bibl index afp ISBN 9781442271647, $95.00; ISBN 9781442271654 pbk, $39.00; ISBN 9781442271661 ebook, $38.99; 54-4356

Hyde, Sarah L. **Schooling in the antebellum South: the rise of public and private education in Louisiana, Mississippi, and Alabama**. Louisiana State, 2016. 212p bibl index afp ISBN 9780807164204, $42.50; ISBN 9780807164228 ebook, contact publisher for price; 54-4345

**Interdisciplinary handbook of trauma and culture**, by Yochai Ataria et al. Springer, 2016. 399p bibl index afp ISBN 9783319294025, $119.00; ISBN 9783319294049 ebook, $89.00; 54-4497

Mann, William J. **The wars of the Roosevelts: the ruthless rise of America’s greatest political family**. Harper, 2016. 609p bibl in-dex ISBN 9780062383334, $35.00; ISBN 9780062383358 ebook, contact publisher for price; 54-4403

Nagy, John A. **George Washington’s secret spy war: the making of America’s first spymaster**. St. Martin’s, 2016. 374p bibl index ISBN 9781250096814, $27.99; ISBN 9781250096821 ebook, contact publisher for price; 54-4404

**Parenting: contemporary clinical perspectives**, ed. by Steven Tuber. Rowman & Littlefield, 2016. 245p bibl index afp ISBN 9781442254817, $75.00; ISBN 9781442254824 ebook, $74.99; 54-4502

**The Progressives’ century: political reform, constitutional government, and the modern American state**, ed. by Stephen Skow-ronek, Stephen M. Engel, and Bruce Ackerman. Yale, 2016. 529p index afp ISBN 9780300204841, $100.00; 54-4474

Ruane, Kevin. **Churchill and the bomb in war and Cold War**. Bloomsbury Academic, 2016. 402p bibl index ISBN 9781472523389, $34.00; ISBN 9781472532169 ebook, contact publisher for price; 54-4426

Shire, Laurel Clark. **The threshold of Manifest Destiny: gender and national expansion in Florida**.  Pennsylvania, 2016. 273p bibl index afp ISBN 9780812248364, $49.95; ISBN 9780812293036 ebook, $49.95; 54-4410

Smith, Jean Edward. **Bush**.  Simon & Schuster, 2016. 808p bibl index ISBN 9781476741192, $35.00; ISBN 9781476741215 ebook, $14.99; 54-4491

Stuhl, Andrew. **Unfreezing the Arctic: science, colonialism, and the transformation of Inuit lands**. Chicago, 2016. 232p bibl index ISBN 9780226416649, $35.00; ISBN 9780226416786 ebook, $35.00; 54-4416

Taylor, Alan. **American revolutions: a continental history, 1750–1804**.  W. W. Norton, 2016. 681p index afp ISBN 9780393082814, $37.50; ISBN 9780393253870 ebook, contact publisher for price; 54-4418

Townsend, Camilla. **Annals of Native America: how the Nahuas of colonial Mexico kept their history alive**. Oxford, 2016. 318p bibl index ISBN 9780190628994, $35.00; ISBN 9780190629007 ebook, contact publisher for price; 54-4373

**The Vietnam War in popular culture: the influence of America’s most controversial war on everyday life: v.1: During the war; v.2: After the war**, ed. by Ron Milam. Praeger, 2016. 2v bibl index afp ISBN 9781440840463, $164.00; ISBN 9781440840470 ebook, contact publisher for price; 54-4419

Weatherford, Jack. **Genghis Khan and the quest for God: how the world’s greatest conqueror gave us religious freedom**. Viking, 2016. 407p bibl index ISBN 9780735221154, $28.00; ISBN 9780735221161 ebook, $14.99; 54-4361

Whitley, Bernard E., Jr.. **Psychology of prejudice and discrimination**, by Mary E. Kite and Bernard E. Whitley Jr. 3rd ed. Routledge, 2016. 713p bibl indexes afp ISBN 9781138947528, $295.00; ISBN 9781138947542 pbk, $119.95; ISBN 9781315623849 ebook, contact pubisher for price; 54-4507

Wickham, Chris. **Medieval Europe**. Yale, 2016. 335p bibl index ISBN 9780300208344, $35.00; ISBN 9780300222210 ebook, con-tact publisher for price; 54-4438

**Women in Civil War Texas: diversity and dissidence in the trans-Mississippi**, ed. by Deborah M. Liles and Angela Boswell. University of North Texas, 2016. 297p index ISBN 9781574416510, $29.95; ISBN 9781574416602 ebook, contact publisher for price; 54-4421

----

Line2: 

----

Author: Choice Staff

----

Author-img: 

----

Byline: 

----

References: 