Title: New community college features added to Choice

----

Date: 2015-10-16

----

Description: 

----

Line1: 

----

Thumb: 

----

Hero: 

----

Text: 

For Immediate Release
Contact: Laura Mullen, 860-347-6933 x124	
lmullen@ala-choice.org	

Choice Debuts Content and Tools Tailored
for the Community College Market

Middletown, CT – October 16, 2015:  Choice, the premier review journal for new English-language books and digital resources for academic libraries, premieres content specifically crafted for community college libraries in the November 2015 issue of Choice magazine and on Choice Reviews Online.

“With two-year colleges currently constituting about 39% of all colleges, the role of the community college library is more important than ever. Today, much of the work there revolves around three main challenges: helping students learn, improving academic and program performance, and promoting innovation,” said Mark Cummings, Editor and Publisher of Choice. “By expanding its attention to instructional resources most appropriate for community college libraries, Choice can help community college libraries meet these challenges.”

With that in mind, Choice’s new community college features include:
•	All titles appropriate for community college libraries are marked with an easily identifiable icon  .
•	The top community college titles each month are listed in a special multipage section at the front of the magazine and in Choice Reviews Online.
•	Editorials addressing topics especially relevant to community colleges will appear in selected issues. Prospective topics to include collection development, inclusiveness, leadership, workforce development, educational technologies, college readiness, information access, OERs, and more.

An editorial by Zoe Fisher, assistant professor, reference, and instruction librarian at Pierce College in Puyallup, WA and a Choice Editorial Board member as well as a member of the Information Literacy Standards Committee for ACRL, entitled “Hey, That’s My Librarian!: Perspectives on Student Engagement from a Community College Librarian” helps kick off Choice’s new targeted content.  As she notes in the November 2015 issue, “Librarians, particularly community college librarians, are uniquely situated to be mentors to adult learners. This is the advantage of ‘doing it all’—we teach, we coordinate, we advise, and we make new connections with students every day.”
Choice looks forward to providing the tools to help community college libraries continue to “do it all,” now and in the future.

----

Line2: 

----

Author: 

----

Author-img: 

----

Byline: 

----

Footnote: 

***
### About Choice
Choice is the publishing unit of the Association of College & Research Libraries (ACRL), a division of the American Library Association. Founded in 1964, Choice has for over 50 years been the premier source for reviews of academic books and digital resources of interest to scholars and students in higher education. Please visit Choice at (link: http://www.choice360.org) for more information.
Contact: Laura Mullen, (tel: 860-347-6933) x124​
(email: lmullen@ala-choice.org)