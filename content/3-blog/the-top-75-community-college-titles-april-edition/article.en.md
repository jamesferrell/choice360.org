Title: The Top 75 Community College Titles: April Edition

----

Subtitle: The best of all the titles appropriate for two-year colleges reviewed in the April issue of Choice.

----

Category: Newsletters

----

Date: 2017-04-27

----

Description: The best of all the titles appropriate for two-year colleges reviewed in the April issue of Choice.

----

Line1: 

----

Thumb: cc-apr-2017.jpg

----

Hero: cc-apr-2017.jpg

----

Hero-caption: 

----

Text: 

### Reference
**The 1950s (1950–1959)**, ed. by Michael Shally-Jensen. Salem Press/Grey House, 2016. 257p bibl index afp ISBN 9781619258860,  $175.00; ISBN 9781619258877 ebook, $175.00;  54-3549

**The 1970s (1970–1979)**, ed. by Michael Shally-Jensen. Salem Press/Grey House, 2016. 373p bibl index afp ISBN 9781619258907,  $175.00; ISBN 9781619258914 ebook, contact publisher for price;  54-3550

Beardsley, Ann. **Historical guide to NASA and the space program**, by Ann Beardsley, C. Tony Garcia, and Joseph Sweeney. Row-man & Littlefield, 2016. 375p bibl ISBN 9781442262867,  $110.00; ISBN 9781442262874 ebook, $109.99;  54-3545

**Creative Commons**. URL:(link: https://creativecommons.org/ text: https://creativecommons.org/ popup: yes) Apr’17,  54-3533

**Eleanor Roosevelt Papers Project**, from George Washington University. URL:(link: https://erpapers.columbian.gwu.edu/ text: https://erpapers.columbian.gwu.edu/ popup: yes) Apr’17,  54-3553

Gray, John. **Hip-hop studies: an international bibliography and resource guide**.  African Diaspora Press, 2016. 1,116p indexes afp (Black music reference series, 7) ISBN 9780984413461,  $150.00;  54-3537

Leahy, Todd. **Historical dictionary of Native American movements**, by Todd Leahy and Nathan Wilson. 2nd ed. Rowman & Lit-tlefield, 2016. 302p bibl afp ISBN 9781442268081,  $90.00; ISBN 9781442268098 ebook, $89.99;  54-3560

Tschen-Emmons, James B. **Buildings and landmarks of medieval Europe: the Middle Ages revealed**. Greenwood, 2016. 347p bibl index ISBN 9781440841811,  $105.00; ISBN 9781440841828 ebook, contact publisher for price;  54-3543

### Humanities
Baggini, Julian. **The edge of reason: a rational skeptic in an irrational world**. Yale, 2016. 262 pages ISBN 9780300208238,  $26.00; ISBN 9780300222081 ebook, contact publisher for price;  54-3670

Christian, Shawn Anthony. **The Harlem Renaissance and the idea of a new Negro reader**. Massachusetts, 2016. 140p index afp ISBN 9781625342003,  $90.00; ISBN 9781625342010 pbk, $25.95;  54-3620

Cohen, Nicole S. **Writers’ rights: freelance journalism in a digital age**.  McGill-Queen’s, 2016. 323p bibl index afp ISBN 9780773547964,  $34.95; ISBN 9780773599772 ebook, contact publisher for price;  54-3597

Flores, Juan Carlos. **Reeaxamining love of wisdom: philosophical desire from Socrates to Nietzsche**.  Cascade Books, 2016. 167p bibl ISBN 9781498237642,  contact publisher for price; ISBN 9781498237628 pbk, $24.00; ISBN 9781498237635 ebook, contact publisher for price;  54-3676

**Foucault / Derrida fifty years later: the futures of genealogy, deconstruction, and politics**, ed. by Olivia Custer, Penelope Deutscher, and Samir Haddad. Columbia, 2016. 234p index afp ISBN 9780231171946,  $105.00; ISBN 9780231171953 pbk, $35.00; ISBN 9780231542999 ebook, $34.99;  54-3678

Foulkes, Julia L. **A place for us: West Side story and New York**. Chicago, 2016. 246p bibl index afp ISBN 9780226301808,  $49.99; ISBN 9780226301945 ebook, $40.00;  54-3645

Freedman, Harry. **The murderous history of Bible translations: power, conflict and the quest for meaning**.  Bloomsbury, 2016. 248p bibl index ISBN 9781632866011,  $28.00; ISBN 9781472921680 ebook, contact publisher for price;  54-3692

Furniss, Maureen. **A new history of animation**. Thames & Hudson, 2016. 464p index ISBN 9780500292099 pbk, $65.00;  54-3649

Moran, Daniel. **Creating Flannery O’Connor: her critics, her publishers, her readers**.  Georgia, 2016. 253p bibl index ISBN 9780820349541,  $39.95; ISBN 9780820349558 ebook, contact publisher for price;  54-3629

Onians, John. **European art: a neuroarthistory**. Yale, 2016. 378p bibl index ISBN 9780300212792,  $75.00;  54-3580

**Paul Klee: irony at work**, ed. by Angela Lampe. Prestel, 2016. 308p bibl ISBN 9783791355436,  $60.00;  54-3587

**The Sage handbook of digital journalism**, ed. by Tamara Witschge et al. SAGE Publishing, 2016. 605p bibl indexes ISBN 9781473906532,  $175.00; ISBN 9781473955066 ebook, contact publisher for price;  54-3605

Schmidt, Leigh Eric. **Village atheists: how America’s unbelievers made their way in a godly nation**.  Princeton, 2016. 337p index afp ISBN 9780691168647,  $35.00; ISBN 9781400884346 ebook, contact publisher for price;  54-3700

**Teaching Buddhism: new insights on understanding and presenting the traditions**, ed. by Todd Lewis and Gary deAngelis. Oxford, 2016. 400p bibl indexes ISBN 9780199373093,  $99.00; ISBN 9780190629151 ebook, contact publisher for price;  54-3701

Thomas, Brook. **The literature of Reconstruction: not in plain black and white**.  Johns Hopkins, 2016. 178p index ISBN 9781421421322,  $40.00; ISBN 9781421421339 ebook, $40.00;  54-3636

Westacott, Emrys. **The wisdom of frugality: why less is more—more or less**. Princeton, 2016. 313p index afp ISBN 9780691155081,  $27.95; ISBN 9781400883301 ebook, contact publisher for price;  54-3685

### Science & Technology
Adams, Mike. **Food forensics: the hidden toxins lurking in your food and how you can avoid them for lifelong health**.  BenBella Books, 2016. 368p index ISBN 9781940363288 pbk, $16.95; ISBN 9781940363462 ebook, $16.99;  54-3779

Benjamin, Darryl. **Farm to table: the essential guide to sustainable food systems for students, professionals, and consumers**, by Darryl Benjamin and Lyndon Virkler. Chelsea Green, 2016. 253p index ISBN 9781603586726,  $49.95; ISBN 9781603586733 ebook, contact publisher for price;  54-3721

Chester, Sharon. **The Arctic guide: wildlife of the far north**.  Princeton, 2016. 542p indexes afp ISBN 9780691139746,  $75.00; ISBN 9780691139753 pbk, $27.95; ISBN 9781400865963 ebook, contact publisher for price;  54-3745

Choffnes, Dan. **Nature’s pharmacopeia: a world of medicinal plants**. Columbia, 2016. 332p bibl index afp ISBN 9780231166607,  $150.00; ISBN 9780231166614 pbk, $75.00; ISBN 9780231540155 ebook, $74.99;  54-3780

**Coral reefs of the eastern tropical Pacific: persistence and loss in a dynamic environment**, ed. by Peter W. Glynn, Derek P. Man-zello, and Ian C. Enochs. Springer, 2016. 657p bibl indexes afp (Coral reefs of the world, 8) ISBN 9789401774987,  $229.00; ISBN 9789401774994 ebook, $179.00;  54-3758

Delgado, Amélia Martins. **Chemistry of the Mediterranean diet**, by Amélia Martins Delgado, Maria Daniel Vaz Almeida, and Sal-vatore Parisi. Springer, 2016. 259p bibl index afp ISBN 9783319293684,  $34.99; ISBN 9783319293707 ebook, $24.99;  54-3781

**The Dyer’s handbook: memoirs on dyeing by a French gentleman-clothier in the Age of Enlightenment translated and contextu-alised**, ed. by Dominique Cardon. Oxbow Books, 2016. 164p bibl (Ancient textiles series, 26) ISBN 9781785702112,  $75.00; ISBN 9781785702129 ebook, contact publisher for price;  54-3768

Elphick, Jonathan. **Birds: a complete guide to their biology and behavior**.  Firefly Books, 2016. 272p index ISBN 9781770857629 pbk, $29.95;  54-3747

**Forest conservation in the Anthropocene: science, policy, and practice**, ed. by V. Alaric Sample, R. Patrick Bixler, and Char Miller. University Press of Colorado, 2016. 336p bibl index afp ISBN 9781607324584,  $87.00; ISBN 9781607325215 pbk, $34.95; ISBN 9781607325215 ebook, $27.95;  54-3734

**Global health care: issues and policies**, ed. by Carol Sue Holtz. 3rd ed. Jones & Bartlett, 2017. 643p bibl index ISBN 9781284070668 pbk, $83.95;  54-3782

Hallett, Mark. **The sauropod dinosaurs: life in the age of giants**, by Mark Hallett and Mathew J. Wedel. Johns Hopkins, 2016. 320p bibl index afp ISBN 9781421420288,  $39.95; ISBN 9781421420295 ebook, $39.95;  54-3762

Hudson, John C. **American farms, American food: a geography of agriculture and food production in the United States**, by John C. Hudson and Christopher R. Laingen. Lexington Books, 2016. 149p bibl index afp ISBN 9781498508209,  $75.00; ISBN 9781498508216 ebook, $74.99;  54-3725

**Introduction to scientific and technical computing**, ed. by Frank T. Willmore, Eric Jankowski, and Coray Colina. CRC Press, 2016. 289p bibl index afp ISBN 9781498745048 pbk, $79.95; ISBN 9781315382395 ebook, $79.95;  54-3792

Kalb, Irv. **Learn to program with Python**.  Apress, 2016. 263p index afp ISBN 9781484218686 pbk, $27.99; ISBN 9781484221723 ebook, contact publisher for price;  54-3793

Lasbury, Mark E. **The realization of Star trek  technologies: the science, not fiction, behind brain implants, plasma shields, quantum computing, and more**.  Springer, 2016.  320p bibl  index  afp  ISBN 9783319409122 pbk, $39.99;  ISBN 9783319409146 ebook, $29.99;  54-3708

Lee Shetterly, Margot. **Hidden figures: the American dream and the untold story of the black women mathematicians who helped win the space race**.  W. Morrow, 2016. 346p bibl index ISBN 9780062363596,  $27.99; ISBN 9780062363619 ebook, contact publisher for price;  54-3714

Macht, Norman L. **The grand old man of baseball: Connie Mack in his final years, 1932–1956**.  Nebraska, 2015. 623p index afp ISBN 9780803237650,  $39.95; ISBN 9780803278967 ebook, contact publisher for price;  54-3809

Millington, Brad. **The greening of golf: sport, globalization and the environment**, by Brad Millington and Brian Wilson. Manches-ter University Press, 2016. 239p bibl index ISBN 9781784993276,  $125.00; ISBN 9781526107039 ebook, contact publisher for price;  54-3810

Murphy, Cait. **A history of American sports in 100 objects**.  Basic Books, 2016. 371p index ISBN 9780465097746,  $29.99; ISBN 9780465097753 ebook, contact publisher for price;  54-3811

Paul, Gill. **A history of medicine in 50 objects**.  Firefly Books, 2016. 224p bibl index ISBN 9781770857186,  $29.95;  54-3785

Roser, Christoph. **“Faster, better, cheaper” in the history of manufacturing: from the Stone Age to lean manufacturing and be-yond**. CRC Press, 2016. 411p bibl index afp ISBN 9781498756303,  $59.95;  54-3777

Ross, Charles K. **Mavericks, money, and men: the AFL, black players, and the evolution of modern football**.  Temple, 2016. 203p bibl index afp ISBN 9781439913062,  $84.50; ISBN 9781439913079 pbk, $19.95; ISBN 9781439913086 ebook, $19.95;  54-3812

Thinard, Florence. **Explorers’ botanical notebook: in the footsteps of Theophrastus, Marco Polo, Linnaeus, Flinders, Darwin, Speke and Hooker**, photographs by Yannick Fourie. Firefly Books, 2016. 175p bibl index ISBN 9781770857636,  $39.95;  54-3737

Thompson, Mark.** A space traveler’s guide to the solar system**.  Pegasus Books, 2016. 257p index ISBN 9781681772394,  $27.95;  54-3719

### Social & Behavioral Sciences
Alexander, Ryan M. **Sons of the Mexican Revolution: Miguel Alemán and his generation**.  New Mexico, 2016. 245p bibl index ISBN 9780826357380,  $95.00; ISBN 9780826357397 pbk, $29.95;  54-3885

’Anānī, Khalīl. **Inside the Muslim Brotherhood: religion, identity, and politics**.  Oxford, 2016. 199p index afp ISBN 9780190279738,  $74.00; ISBN 9780190279745 ebook, contact publisher for price;  54-3977

**The Black Panthers: portraits from an unfinished revolution**, ed. by Bryan Shih and Yohuru Williams. Nation Books, 2016. 272p index ISBN 9781568585550 pbk, $24.99; ISBN 9781568585567 ebook, contact publisher for price;  54-3904

**Black Power 50**, ed. by Sylviane A. Diouf and Komozi Woodward. New Press, 2016. 144p bibl ISBN 9781620971482 pbk, $30.95; ISBN 9781620971512 ebook, contact publisher for price;  54-3905

Black, Jeremy. **The Holocaust: history and memory**.  Indiana, 2016. 296p index afp ISBN 9780253022042,  $80.00; ISBN 9780253022141 pbk, $28.00; ISBN 9780253022189 ebook, $27.99;  54-3847

Caplan, Lincoln. **American justice 2016: the political Supreme Court**.  Pennsylvania, 2016.  ISBN 9780812248906,  $24.95; ISBN 9780812293722 ebook, $19.95;  54-3948

Cornils, Ingo. **Writing the revolution: the construction of “1968” in Germany**.  Camden House, 2016. 315p bibl index ISBN 9781571139542,  $90.00; ISBN 9781782048299 ebook, contact publisher for price;  54-3940

**Couple relationships in the middle and later years: their nature, complexity, and role in health and illness**, ed. by Jamila Bookwala. American Psychological Association, 2016. 358p bibl index ISBN 9781433822094,  $69.95; ISBN 9781433822100 ebook, contact publisher for price;  54-4023

Fine, Ben. **Marx’s Capital**, by Ben Fine and Alfredo Saad-Filho. 6th ed. Pluto, 2016. 192p bibl index afp ISBN 9780745336039,  contact publisher for price; ISBN 9780745336978 pbk, $22.00; ISBN 9781783719723 ebook, contact publisher for price;  54-3996

**Grandfathers: global perspectives**, ed. by Ann Buchanan and Anna Rotkirch. Palgrave Macmillan, 2016. 327p bibl index ISBN 9781137563378,  $99.99; ISBN 9781137563385 ebook, $79.99;  54-4035

**Handbook of sexual orientation and gender diversity in counseling and psychotherapy**, ed. by Kurt A. DeBord et al. American Psychological Association, 2017. 468p bibl filmography index ISBN 9781433823060,  $79.95;  54-4025

Litt, Paul. **Trudeaumania**.  UBC Press, 2016. 412p index ISBN 9780774834049,  $39.95; ISBN 9780774834087 ebook, contact publisher for price;  54-3922

Lucas, George. **Ethics and cyberwarfare: the quest for responsible security in the age of digital warfare**.  Oxford, 2016. 187p bibl index ISBN 9780190276522,  $34.95; ISBN 9780190276553 ebook, contact publisher for price;  54-3988

Marek, Christian. **In the land of a thousand gods: a history of Asia Minor in the ancient world**, by Christian Marek in collaboration with Peter Frei; tr. by Steven Rendall. Princeton, 2016. 797p bibl indexes afp ISBN 9780691159799,  $49.50;  54-3865

Martinez, Donna. **Urban American Indians: reclaiming Native space**, by Donna Martinez, Grace Sage, and Azusa Ono. Praeger, 2016. 157p bibl index afp ISBN 9781440832079,  $60.00; ISBN 9781440832086 ebook, contact publisher for price;  54-4039

McAuliffe, Mary. **When Paris sizzled: the 1920s Paris of Hemingway, Chanel, Cocteau, Cole Porter, Josephine Baker, and their friends**.  Rowman & Littlefield, 2016. 329p bibl index afp ISBN 9781442253322,  $29.95; ISBN 9781442253339 ebook, $28.99;  54-3946

McDougall, Walter A. **The tragedy of U.S. foreign policy: how America’s civil religion betrayed the national interest**.  Yale, 2016. 408p bibl index ISBN 9780300211450,  $30.00; ISBN 9780300224511 ebook, contact publisher for price;  54-3923

Phillips, Joshua D. **Homeless: narratives from the streets**.  McFarland, 2016. 196p bibl index ISBN 9781476664576 pbk, $29.95; ISBN 9781476625270 ebook, contact publisher for price;  54-4041

**Police use of force: important issues facing the police and the communities they serve**, ed. by Michael J. Palmiotto. CRC Press, 2016. 196p bibl index afp ISBN 9781498732147,  $47.96; ISBN 9781315369921 ebook, $41.97;  54-4042

Robinson, Chase F. **Islamic civilization in thirty lives: the first 1,000 years**.  California, 2016. 272p bibl index ISBN 9780520292987,  $29.95;  54-3900

Scott, Michael. **Ancient worlds: a global history of antiquity**.  Basic Books, 2016. 411p index ISBN 9780465094721,  $29.99; ISBN 9780465094738 ebook, contact publisher for price;  54-3867

Shelby, Tommie. **Dark ghettos: injustice, dissent, and reform**.  Belknap, Harvard, 2016. 340p index afp ISBN 9780674970502,  $29.95;  54-4045

Taylor, Rabun. **Rome: an urban history from antiquity to the present**, by Rabun Taylor, Katherine Wentworth Rinne, and Spiro Kostof. Cambridge, 2016. 432p bibl index ISBN 9781107013995,  $120.00; ISBN 9781107601499 pbk, $39.99; ISBN 9781316680056 ebook, $32.00;  54-3868

Thrush, Coll. **Indigenous London: Native travelers at the heart of empire**.  Yale, 2016. 310p index afp ISBN 9780300206302,  $38.00; ISBN 9780300224863 ebook, contact publisher for price;  54-3936

**The US labor market: questions and challenges for public policy**, ed. by Michael R. Strain. AEI Press, 2016. 289p ISBN 9780844750071,  $101.00;  54-3846

United States Military Academy. **West Point history of World War II, v.2**, ed. by Clifford J. Rogers, Ty Seidule, and Steve R. Waddell. Simon & Schuster, 2016. 370p index ISBN 9781476782775,  $55.00; ISBN 9781476782744 ebook, $41.99;  54-3857

Yanoso, Nicole Anderson. **The Irish and the American presidency**.  Transaction, 2016. 272p index afp ISBN 9781412863995,  $69.95; ISBN 9781412863704 ebook, contact publishers for price;  54-3932

----

Line2: 

----

Author: Choice Staff

----

Author-img: 

----

Byline: 

----

References: 