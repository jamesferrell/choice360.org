Title: Webinar of the Week 3-20-2017

----

Subtitle: 

----

Category: 

----

Date: 2017-03-20

----

Description: 

----

Line1: 

----

Thumb: 

----

Hero: 

----

Hero-caption: Image from the Library of Congress.

----

Text: 

## Why this webinar?
Midway through National Women’s History Month, it seems like the perfect time to bring back this webinar with one of the founders of the National Women’s History Project.

Before this webinar, I didn’t know the story behind the National Women’s History Project or how Molly Murphy MacGregor and her colleagues’ tireless work secured March as National Women’s History Month. 

Now I do. 


## What it’s really about.
The following anecdote, condensed from about 23:30, illustrates what’s so great about this webinar:
 
### It’s 1980 in Santa Rosa, California. 

Molly Murphy MacGregor’s working for the California Department of Education. She’s been tasked with evaluating the gender bias in elementary school libraries in her county. To get a picture of the bias in the collections, she searches for books that have something to do with women or women’s history. She finds that each collection has between three and seven books about women or women’s history. Those books haven’t been checked out  in between five and fifteen years.
*****
>>###<font face="Magneto, Georgia, serif" color="#882d19" >She finds each collection has between three and seven books about women or women’s history. The books haven’t been checked out once in anywhere from five to fifteen years</font>

*****
As a teacher, Molly suspects the reason those books aren’t being checked out is that teachers aren’t assigning them. And the reason teachers aren’t assigning them? Probably, the teachers themselves don’t know much about women’s history. 
 
It’s at this point Molly and her colleagues decide to start working toward a local women’s history week in Santa Rosa, which is just the first step in the journey to designating March as National Women’s History Month.


## View the Webinar.

<iframe width="724" height="480" src="https://www.youtube.com/embed/BRR9ROFjhKI" frameborder="0" allowfullscreen></iframe>

----

Line2: 

----

Author: Mark Derks

----

Author-img: derks-headshot.jpg

----

Byline: Mark Derks is the digital media coordinator at Choice, where he runs the webinar program.

----

References: 