Title: Editors' Picks for April

----

Subtitle: 10 reviews handpicked from the latest issue of Choice.

----

Category: Newsletters

----

Date: 2017-04-20

----

Description: 10 reviews handpicked from the latest issue of Choice.

----

Line1: 

----

Thumb: ep-apr-2017.jpg

----

Hero: ep-apr-2017.jpg

----

Hero-caption: 

----

Text: 

ʻAnānī, Khalīl. **Inside the Muslim Brotherhood: religion, identity, and politics**. Oxford, 2016. 199p index afp ISBN 9780190279738, $74.00; ISBN 9780190279745 ebook, contact publisher for price.

*Inside the Muslim Brotherhood* is a rich study of this socioreligious, economic, and political movement, which endured decades of ups and down and remains an influential and resilient player in Egypt and the Middle East. Through a detailed analysis of internal documents, deep knowledge of its history, and interviews with Brotherhood’s members, ʻAnānī (Doha Institute, Qatar) presents readers with a unique study that captures internal structure and ideology informed by organizational theory, social movement theory, psychology, and sociology of Islam. Readers learn about the Brotherhood’s origins, growth, and expansion into one of the most influential Islamic movements of our time. The founder Hassan al-Banna’s vision still remains vital to the Brotherhood through careful multistage recruitment strategy, socialization, and mobilization channels. Figures and tables provide powerful visual description of the Brotherhood. Al-Banna’s framework of Islamic identity, the Jama’a paradigm that shows how aims translate into objectives and mission to be carried out by the brotherhood, and strategies of individual’s provide important background to understanding the internal struggles and successes of this movement under turbulent politics. This is a must-read for understanding Islamist politics of our time. **Summing Up:** Essential. Lower-division undergraduates through faculty. *—B. A. Yesilada, Portland State University*

***

Bergen, Benjamin K. **What the f: what swearing reveals about our language, our brains, and ourselves**. Basic Books, 2016. 271p bibl index ISBN 9780465060917, $27.99; ISBN 9780465096480 ebook, contact publisher for price.

Bergen (linguistics and cognitive science, Univ. of California, San Diego) is a popular writer who appears regularly on NPR, the *Brain Science Podcast*, and elsewhere. Here he offers an unusual book about cursing and language processing. Focusing on the f word and other taboo expletives, Bergen applies his dry sense of humor to chronicling the history and cross-cultural, neuropsychological, and psycholinguistic ramifications of the use, and non-use, of profanity. In 11 chapters, the author meanders through an array of topics loosely associated with the concept of profanity—for example, what makes a four-letter word, the paradox of profanity—and he includes an organizing framework of four types of swear words: gesturing, religiosity, grammar, and context. Including endnotes and a fairly comprehensive bibliography of published works on this topic, this book is a surprisingly engaging introduction to a topic rarely discussed or examined. **Summing Up:** Highly recommended. Lower-division undergraduates and above; general readers. *—G. C. Gamst, University of La Verne*

***

**Evolutionary theory: a hierarchical perspective**, ed. by Niles Eldredge et al. Chicago, 2016. 385p index afp ISBN 9780226426051, $105.00; ISBN 9780226426228 pbk, $35.00; ISBN 9780226426198 ebook, $35.00.

This excellently "integrated" and edited volume of 14 chapters (plus introductory and concluding chapters) is the essential resource for any individual seeking to understand the central role that hierarchical thinking has played over the past several centuries in efforts to understand relationships between and change within and among organisms. With a strong emphasis on speciation and unifying theoretical and philosophical perspectives, these chapters combine the ecological (spatial, system, "niche construction," and dynamic relationships) and genealogical (temporal, lineage, "niche evolution," and emergent properties) aspects of evolution so often studied in isolation. Nested hierarchies of individuals, species, niches, populations, and communities interacting causally with genetic and epigenetic developmental and ecological processes are used to understand dichotomies such as macro and microevolution, tempo and mode, and pattern and process in evolution. Many chapters, including the introduction, highlight these themes in a historical context, an approach that integrates the chapters to reveal just how deeply rooted hierarchical perspectives are in the quest to understand organismal relationships and evolution. Dual categories, such as evolution and development, pattern and process, and nature and nurture begin to fall away in the light of the approach expounded in this illuminating volume. **Summing Up: **Essential. Graduate students, faculty, and professionals. *—B. K. Hall, Dalhousie University*

***

Griffin, Ronald C. **Water resource economics: the analysis of scarcity, policies, and projects**. 2nd ed. MIT, 2016. 476p bibl index afp ISBN 9780262034043, $96.00; ISBN 9780262334044 ebook, $96.00.

This is the second edition of a highly useful textbook for upper-division undergraduates or masters-level students in public policy or economics. In the second edition, Griffin adds several useful sections, e.g., details on active water markets in Australia and California (written by experts in respective areas), block water pricing, and new appendixes including a very useful one on input-output analysis. It begins with basic economic concepts such as supply and demand and builds to more complicated models such as dynamic efficiency, while nicely merging these economic concepts with real world applications in water policy. This makes it accessible without sacrificing rigor or nuance. The context is that of the US (other than the section on Australia), not the developing world. The sections on risk, policy analysis, and dynamic efficiency are especially well written. The treatment is fairly quantitative; end-of-chapter exercises, accompanied with spreadsheets and programming code on the author’s website, make this an ideal resource for pedagogical purposes. This book is the fruit of Griffin’s highly prolific career in research and teaching water resource economics, and is a great resource for faculty and students of water economics and policy. **Summing Up:** Highly recommended. Upper-division undergraduates through faculty. *—A. M. Chaudhry, California State University*

***

Hassner, Ron E. **Religion on the battlefield**. Cornell, 2016. 222p index afp ISBN 9780801451072, $24.95; ISBN 9781501703690 ebook, contact publisher for price.

Because of the influence of realism in 20th-century international relations scholarship, the role of morality has been deemphasized while the influence of religion remained largely neglected until the end of the Cold War. When scholars realized that religion was not fading, as the dominant secularist paradigm had claimed, they began to address the role of religion as a determinant in domestic and international politics and more specifically as an influence in statecraft. As a result, a significant literature has emerged on religion and international affairs. Hassner’s book is an important addition to this scholarship, since it addresses the neglected theme of how religious beliefs and practices affect the conduct of war. Using 20th-century military conflicts, Hassner (UC Berkeley) examines how religion influenced the source, timing, location, leadership, and manner of war. He shows how religious factors constrain decision making on the battlefield. Chapter 6 focuses on the sacred elements of time, space, authority, and intelligence in the 2003–09 US military operations in Iraq. This accessible, well-written book will be of special interest to national security specialists and scholars of religion and warfare. Recommended for all academic libraries. **Summing Up:** Essential. General readers; upper-division undergraduates through faculty. *—M. Amstutz, Wheaton College*

***

Hill, Shirley A. **Inequality and African-American health: how racial disparities create sickness**. Policy Press, 2016. 194p bibl index ISBN 9781447322818, $110.00; ISBN 9781447322825 pbk, $32.95; ISBN 9781447322856 ebook, contact publisher for price.

This relatively short book gives a panoramic view of inequality in African American health. The book's examination of racial disparities in health is both horizontal and vertical, both contemporary and historical. Sociologist Hill (Univ. of Kansas) illustrates contemporary racial disparities in health in a deeper historical background, tracing racism and negative health outcomes for African Americans back to the slavery period, revealing how historical and structural inequalities maintain and foster contemporary disparities. The author also goes beyond medical data to analyze inequality in African American health in a wider social context, referencing the factors of prejudices of the medical profession, health policies, economic status, incarcerations, sexuality and marriage, and child-rearing patterns. Racial disparities in all of these factors exert negative impact on the health of African Americans and are widely accepted as major causes of health crises. The book's comprehensive coverage of racial disparities provides abundant information to help readers grasp an overall view of this issue, as well as premises for future research. A sufficiently broad, specific, timely, and balanced book on African American health issues for anyone. **Summing Up: **Highly recommended. All levels/libraries. *—A. Y. Lee, George Mason University*

***

Lee Shetterly, Margot. **Hidden figures: the American dream and the untold story of the black women mathematicians who helped win the space race**. W. Morrow, 2016. 346p bibl index ISBN 9780062363596, $27.99; ISBN 9780062363619 ebook, contact publisher for price.

In her debut book, Shetterly profoundly profiles four female African American employees of the National Aeronautics and Space Administration (NASA) Langley Research Center. Prior to its widespread adoption of electronic computers, the National Advisory Committee for Aeronautics (NACA) employed “human computers” (all women) to perform calculations assigned by engineers (all men). Due to their location on the Langley campus, the African American women computers (the group included Dorothy Vaughan, Mary Jackson, Katherine Johnson, and Christine Darden) were known as the “West Computers.” These women were segregated from other offices until the pivotal year of 1958, when NACA became NASA. Shetterly expertly details the women's struggles against organizational segregation and discrimination, most notably the inroads that the West Computers made in obtaining assignments that had previously been limited to male or white employees, including editorial board participation and authorship of technical reports used for Apollo and the Space Shuttle programs. Shetterly contrasts these events with desegregation legislation opposition and the resulting closing of schools in Little Rock, Arkansas, and Hampton, Virginia (home of Langley). The overarching theme is that whether at NASA or nationally, the potential of US success was negatively impacted by segregation. This work is an important assessment of women’s roles in the sciences and US segregation. **Summing Up:** Essential. All readers. *—K. D. Winward, Central College*

***

**The Oxford handbook of philosophy of science**, ed. by Paul Humphreys; advisory editors Anjan Chakravartty, Margaret Morrison, and Andrea Woody. Oxford, 2016. 917p bibl index afp ISBN 9780199368815, $175.00; ISBN 9780199368815 ebook, contact publisher for price.

What, if anything, unifies the sciences? As science fields, following historical trajectories, become increasingly specialized and use equally specialized models, experimental approaches, technologies, computational procedures, and ways of theorizing (if there is not methodological unity), could there be congruity provided by philosophical underpinnings? This handbook, edited by Humphreys (Univ. of Virginia), contains 42 chapters and is organized in three sections: “Overviews,” “Traditional Topics,” and “New Directions,” and offers a stimulating array of topics that demonstrate the vast richness and utility of the philosophy of science. But while the diversity of essays in this collection indicates that the philosophy of science too has become increasingly specialized, contributor Stathis Psillos (Univ. of Athens, Greece) persuasively argues that a general philosophy of science still has merit and currency. These essays are highly accessible and engaging for both those who are accomplished in the field and those interested individuals who are outside of the field. Although some chapters (may) require more technical knowledge than others, each philosopher provides appropriate background to foster understanding among a wider readership. As a collection that reveals the diversity and unity of both scientific and philosophical endeavor, this is an essential book. **Summing Up:** Essential. Upper-division undergraduates and above; faculty and professionals. *—Z. B. Johnson, Lake Erie College*

***

**Police use of force: important issues facing the police and the communities they serve**, ed. by Michael J. Palmiotto. CRC Press, 2016. 196p bibl index afp ISBN 9781498732147, $47.96; ISBN 9781315369921 ebook, $41.97.

As the editor states in the preface, this timely text is intended to supplement the existing literature on the use of force by police, which is horribly dated and often skewed. Palmiotto (Wichita State Univ.) has done an impressive job as both editor and contributor. His first three chapters and conclusion provide an excellent overview of the nature and scope of the problem, and the contributors offer a wide array of perspectives that give readers several different vantage points from which to view the issue—the major strength of this book. The law enforcement perspective is well represented, but this book (unlike many of its predecessors) considers many facets of the issue from both social scientific and legalistic perspectives. The text is written in a concise and accessible style that is well organized and without unnecessary filler, making it a good choice for both graduate and undergraduate collections. **Summing Up:** Highly recommended. All levels/libraries. *—A. J. McKee, University of Arkansas Monticello*

***

Wiley, James. **Politics and the concept of the political: the political imagination**. Routledge, 2016. 300p bibl index ISBN 9781138185814, $150.00; ISBN 9781138185821 pbk, $49.95; ISBN 9781315644233 ebook, contact publisher for price.

This book is an ambitious attempt to correct political theory that aims to "overcome" politics by subordinating it to something else, like moral theory. Instead, Wiley suggests that the accounts of "the political" offered by a number of theorists, including Weber, Wolin, and Mouffe, show how to articulate a "politics-centered" political theory. These theorists together present multiple conceptions of "the political" based in either the state, the polis, or society, and each account allows for a non-subordinate relationship between politics and ethics or philosophy. (Wiley identifies these relationships as "asymmetrical," but does not get much mileage out of the term.) By "putting politics back in" to political theory, Wiley generates a number of wide-ranging practical implications, from countering International Relations theorists' myopic focus on the state to cataloging 19th-century populism's move from an economic to a political perspective on the economy. Theorists and political scientists alike may quibble with these kind of arguments in the book—and there are many of them—but overall, Wiley presents an important model of how to connect political theory to the rest of the discipline. Put another way, the book may not have much to offer social scientists uninterested in high-level theorizing, but is essential reading for political theorists. **Summing Up:** Essential. Graduate students and faculty. *—R. J. Meagher, Randolph-Macon College*

----

Line2: 

----

Author: Choice Staff

----

Author-img: 

----

Byline: 

----

References: 