Title: Internet Resources: May Edition

----

Subtitle: Selected reviews of digital reference resources from the May issue of Choice.

----

Category: Newsletters

----

Date: 2017-05-04

----

Description: Selected reviews of digital reference resources from the May issue of Choice.

----

Line1: 

----

Thumb: may-ir-2017.jpg

----

Hero: may-ir-2017.jpg

----

Hero-caption: 

----

Text: 

**Americana**. Adam Matthew, 2017. Contact publisher for academic pricing based on FTE, purchase history, and Carnegie Classification, discounted from the list price of each component collection. 
(link: http://www.americana.amdigital.co.uk/ text: http://www.americana.amdigital.co.uk/ popup: yes)

[Visited Feb'17] Adam Matthew created this amalgam of seven separately sold, curated collections, offering institutions that already own some of the resources an opportunity to upgrade at a discount. The collective *Americana* database includes *American History, 1493-1945*, from the Gilder Lehrman Institute of American History ((link: http://www.choicereviews.org text: CH, Jun'15, 52-5108 popup: yes)); American Indian Histories ((link: http://www.choicereviews.org text: CH, Mar'14, 51-3592 popup: yes)); American West ((link: http://www.choicereviews.org text: CH, May'09, 46-4787 popup: yes)); Everyday Life and Women in America, covering 1800–1920 ((link: http://www.choicereviews.org text: CH, Oct'08, 46-0654 popup: yes)); Popular Culture in Britain and America, 1950–1975 ((link: http://www.choicereviews.org text: CH, Jun'12, 49-5439 popup: yes)); Slavery, Abolition and Social Justice, 1490–2007 ((link: http://www.choicereviews.org text: CH, Mar'10, 47-3581 popup: yes)); and Virginia Company Archives ((link: http://www.choicereviews.org text: CH, Oct'10, 48-0641 popup: yes)). The cross-searchable platform provides researchers with primary source materials totaling more than 2.5 million pages from handwritten or typescript documents plus maps, objects, photos, and videos.

The unified interface is similar in design to the component collections, featuring a standardized search of title and author fields or place of publication, supporting truncation, proximity searching, date limiting, and filtering by collection, library, or document type. One can search any of 52 source locations that contributed materials, such as the UK's National Archives, Anti-Slavery International, Cambridge University Library, or Bank of England, and such US repositories as Bowling Green State University, NASA, or Newberry Library. Researchers can apply four-dozen different document-type limiters (e.g., art work, book, broadside, correspondence, court records, diary, journal, manuscript, newspaper, photography, statistics, or video). Search results display in a tabular view (tallying total numbers). Results show thumbnail images, titles, dates, and collections, and two data visualizations are helpful—one a pie chart breaking down results by century and collection, the other a frequency time line. Depending on the condition of the original, image and video resolution quality are excellent. The platform's Popular Searches highlights hundreds of subjects and places, demonstrating the possibilities of simple keyword searches (e.g., accidents, Black Panther Party, Paris, Niagara Falls). A browsable table of all documents can be arranged by title (default), by date (somewhat problematic due to non-standard formatting), or by collection. Item records provide, among other fields, physical description, subjects, and library locations. Displayed results link to the original collection interface with excellent zoom access, downloadable PDF versions, and, often, transcriptions with highlighted keywords. Help is limited to a Quick Tour overview of the site.

The Explore section provides map galleries for five subset collections, while Additional Resources, for example,  present a tribes and nations outline, social justice tutorials, and archival videos from popular culture collections. The Chronology section offers links to sometimes interactive chronologies. The separate collections feature a variety of such materials not immediately apparent until one leaves the collective *Americana* website. Licensing permits unlimited users, remote access, downloading, and scholarly sharing. Outright purchase requires an annual access charge. Carefully curated digital collections like these, while costly, can be expected to increase in value for teaching and research. **Summing Up:** Highly recommended. Upper-level undergraduates through researchers/faculty; teachers. *—R. A. Aken, University of Kentucky*

***

**Archives of Sexuality & Gender: LGBTQ History and Culture since 1940**. Gale, part of Cengage Learning, 2017. Contact publisher for pricing. 
(link: http://www.gale.com/archives-of-sexuality-and-gender-lgbtq/ text: http://www.gale.com/archives-of-sexuality-and-gender-lgbtq/ popup: yes)

[Visited Feb'17] Primary source documents assembled under *LGBTQ History and Culture since 1940* can be found in the *Archives of Sexuality and Gender* interface on the *Gale Primary Sources* platform. Among the 19 archival collections included here at the time of this review are documents from ACT UP, the Lesbian Herstory Archives, the Mattachine Society of New York's records (held by the New York Public Library), and the Gay Activists Alliance, among many others. An international perspective is evident in materials also drawn from the Canadian Lesbian and Gay Archives and from repositories in the UK (providing more than 40,000 pages of publications from such sources as the biweekly *Gay News*).

Collections can be browsed by subject, and retrieved items can be limited by document type, content type, language, and source library. Each includes a short Overview section with an unsigned essay that gives context to the collection, and the Collection Facts section indicates years of coverage, the extent of the manuscript collection, the source institution, and the language of the materials included. While the majority are in English, some items can be found in Polish, Russian, Japanese, French, Spanish, and other languages. If a collection contains documents from multiple sources, such as newsletters, a publication title tab lists the titles. Delving into a particular collection’s contents can be accomplished via a rotating carousel, which also allows users to view (and know the number of) all the documents contained in that collection. Depending on the structure of the original, documents may also be organized in subsections by subject.

At any point, users can search across multiple collections and limit search results by content type, subject, or publication year, or by searching from within the displayed results. Two unique visualization tools are term clusters (users can choose to display terms as a wheel or in tiles) and term frequency. These can be very helpful to find new keywords and produce a ranked list of subjects associated with the search results; a help link gives more information about using these tools. Search results are displayed and can be further filtered by document type (e.g., monographs, newspapers and periodicals, or manuscripts). Users can manage their results by means of several options, including creating folders and seeing the citation associated with an item. The extent of the collections is truly outstanding overall, and individual collections range from a few hundred to a quarter million items. *Archives of Sexuality and Gender* is an unparalleled resource for access to primary source documents of concern to LGBTQ scholars and activists alike. **Summing Up:** Highly recommended. All libraries. All levels. *—E. M. Bosman, New Mexico State University Library*

***

**Brain Pickings: An Inventory of the Meaningful Life**, from Maria Popova. 
(link: https://www.brainpickings.org/ text: https://www.brainpickings.org/ popup: yes)

[Visited Feb'17] At a time when many mainstream publications such as *The Washington Post* are reducing or even completely eliminating book reviews, *Brain Pickings* is essentially a one-woman literary review comparable to the *New York Review of Books* or another one-person operation, M. A. Orthofer’s The Complete Review Guide to Contemporary World Fiction ((link: http://www.choicereviews.org text: CH, Oct’16, 54-0469 popup: yes)). Despite the fact that it is almost solely the work of writer Popova, updates are frequent enough that readers can subscribe to a weekly highlights newsletter. Popova has written for numerous publications (e.g., *Huffington Post* or *BusinessWeek*) and is an MIT “Futures of Entertainment Fellow.” She has an erudite but approachable style that draws the reader in.

Popova describes her work as providing intellectual "Legos" that readers can use and combine in their own creative work, and she includes articles and reviews that range across the sciences, social sciences, and humanities. One recent example is a piece on Olivia Laing's recent memoir *The Lonely City*, and Hannah Arendt's exploration of the role that loneliness and isolation can play in creating the conditions for tyranny in her 1951 *Origins of Totalitarianism*. More playful items include an infographic visualizing the sleep habits of 37 writers versus their literary productivity (rising early led to more award-winning works, but lower total output). The site is searchable and browsable by subject, and includes all ten years of material since its 2006 inception. **Summing Up:** Highly recommended. All readership levels. *—J. Stevens, George Mason University*

***

**Google Earth**. 
(link: https://www.google.com/earth/ text: https://www.google.com/earth/ popup: yes)

[Revisited Feb'17] Google Earth ((link: http://www.choicereviews.org text: CH, Jan'09, 46-2443 popup: yes)) continues to evolve as a preeminent geographic information system. The resolution levels on the satellite pictures have improved greatly in the version reviewed here (7.1.7.2606, released October 2016). The street-level pictures taken from roving vehicles give the impression of driving through these virtual geographies, now available for even the smallest locales. The functionality of the software and ability to navigate the world continues to be superior to all extant products. Typing a locality name or address in the Search box (familiar to many as Fly To) enables users to travel from one part of the globe to another in a blink of the eye. Users may show images with an extensive variety of layers (e.g., borders, roads, satellite imagery updates), thus adding rich texture to the learning experience.

*Google Earth* developers obtain information from diverse sources such as *Wikipedia*, the National Geographic Society, NASA, and a host of other government agencies and private entities. New is a Global Awareness feature that tracks the work of international organizations such as the World Wildlife Fund, UNICEF's Water and Sanitation project, Greenpeace, and the ARKive endangered species project. A Show Historical Imagery option reveals mosaics over a 30-year period depicting Earth's changes. One can map business entities by typing Find Businesses in the Search box or search for specific businesses without knowing their addresses. Supported GPS devices allow users to track, import, and change map data. In addition to terrestrial visualizations, *Google Earth* continues to feature an extensive astronomical chart with images from sources including the Hubble Space Telescope, making it a virtual planetarium. The application software and images load and resolve quickly. Excellent tutorials and help resources, and a tip guide that begins each new session, are all informative even for seasoned *Google Earth* travelers. **Summing Up:** Highly recommended. All libraries. All levels. *—J. C. Stachacz, Wilkes University*

***

**Human Rights Online**. Alexander Street, 2017. Annual academic subscription ranges from $1,100.00 to $4,300.00.
(link: http://alexanderstreet.com/ text: http://alexanderstreet.com/ popup: yes)

[Visited Feb'17] Whether one is paying attention to current news coming out of Aleppo or Syria or investigating historical events such as the Nazi Holocaust or the Khmer Rouge's Cambodian Killing Fields, the details of fundamental human rights surrounding ethnicity, gender, and religion are often key to understanding the story. *Human Rights Online* pieces together that story of human rights violations and atrocities from across the world, covering 1900 to 2010. This new Alexander Street database contains well over 4,000 records featuring more than 60,000 full-text pages of primary and secondary sources and formats ranging from government reports, letters, and maps to video documentaries, books, news reports, and more.

A highlight of the collection is the ease of exploring the database itself. By browsing user-friendly categories (or using the interface's intuitive search functions), viewers are able to select resources, for example, that focus on places (China, Haiti, Turkey, etc.), people (Slobodan Milosevic, Augusto Pinochet, Woodrow Wilson), disciplines (diplomacy, geography, politics), themes (international response, post-conflict support, transitional justice), events (Argentina's Dirty War, global counterterrorism, the Israeli-Palestinian conflict), and more. In terms of content, Rwanda, Armenia, and Bosnia form the bulk of materials, with Rwanda alone contributing more than 1,700 records. A keyword search for *Darfur*, for example, yielded more than 100 results, with a mix of reports from sources including the United Nations, Amnesty International, Human Rights Watch, and US congressional hearings, along with clips from documentaries, images, and full-text books. In perusing the site, some unique items were found that are not easily accessible elsewhere.

The site features an interactive time line and maps, but the primary source materials (notably interviews, first-hand accounts, and news reports) remain the star attraction among the contents. Considering current events, this curated collection offers students and researchers a range of excellent primary sources and coverage extensive enough to suit historical researchers and those seeking context for more recent news events. **Summing Up:** Highly recommended. All readership levels. *—J. A. Hardenbrook, Carroll University*

***

**Open Modernisms**, From Stephen Ross and J. Matthew Huculak, University of Victoria.
(link: http://openmods.uvic.ca/ text: http://openmods.uvic.ca/ popup: yes)

[Visited Feb'17] This free platform allows modernist instructors and scholars to find, remix, and create custom course packs and anthologies of primary source materials. As the name implies, the site’s content focuses on works in the humanities from approximately 1890-1940. Users are encouraged to add to the novels, essays, criticism, poems and other documents in the collection, so the 300+ items currently available should increase rapidly as the user community grows. All items are in the public domain in Canada, where the site originates. The interface facilitates uploading source documents or course descriptions, syllabi, table of contents, or other pedagogical materials useful in a course pack. The straightforward interface makes it easy to find, select, and arrange particular items for an anthology and then download the personalized compilation. The download consists of a series of ordered PDF files suitable for digital distribution or printing. Anthologies created on the site remain for others to use or rearrange; all are licensed under a Creative Commons license.

Humanities postdoctoral fellows Matthew Huculak (Univ. of Victoria, Canada) and Claire Battershill (Simon Fraser Univ., Canada) act as project leaders for the site, and associate professor of English Stephen Ross (Univ. of Victoria, Canada) is credited along with Huculak for developing the course-pack builder. Because the platform is open source, based on a customized Islandora module with code available on the collaborative software-builder site *GitHub*, it is adaptable for use in other subject areas, like the open-source Romantics (1750-1850) project currently in development at Simon Fraser. Overall, this site serves well as a tool for faculty to use in developing courses for undergraduates or graduates. **Summing Up:** Recommended. Faulty and instructors or academic technology administrators. *—M. F. Jones, Brevard College*

***

**Silent Film Sound & Music Archive**, from Kendra Preston Leonard.
(link: http://www.sfsma.org/ text: http://www.sfsma.org/ popup: yes)

[Visited Feb'17] *The Silent Film Sound & Music Archive* (*SFSMA*) was initiated in 2014 to collect and save music from the silent movie era, c. 1894–1929. The archive includes sheet music, instruction manuals, bibliographies, audio samples, and podcasts. The majority of materials are in the public domain, but the site also contains some resources from the HathiTrust Digital Library ((link: http://www.choicereviews.org text: CH, May'10, 47-4750 popup: yes)). Users can search these various types of content by creator, title, publisher, format, and instrumentation; scrolling through the site's tag cloud is also an effective way to find materials. One can easily discover, for example, the score from the controversial 1915 film *The Birth of a Nation* along with music by or about Charlie Chaplin. Comments are disabled on the site's *WordPress* platform, although users are encouraged to contact the site director—musicologist Leonard—to contribute supplementary information about a resource. One concern is that some scanning is of inconsistent quality; for instance, a 206-page book by Erno Rapee, *Encyclopedia of Music for Pictures* (1925), has a fair number of illegible pages. As more resources are added and better scanning standards followed, the archive will continue to grow in value. The *SFSMA* should be commended—along with other initiatives, such as Indiana University's digital collection I*N Harmony: Sheet Music from Indiana* at (link: http://webapp1.dlib.indiana.edu/inharmony/ text: http://webapp1.dlib.indiana.edu/inharmony/ popup: yes)—for the concerted effort by skilled volunteers to preserve music history. **Summing Up:** Highly recommended. All readership levels. *—J. A. Badics, Eastern Michigan University*

***

**Transparency International**. 
(link: http://www.transparency.org/ text: http://www.transparency.org/ popup: yes)

[Revisited Feb'17] Global watchdog Transparency International ((link: http://www.choicereviews.org text: CH, Nov’06, 44-1600 popup: yes)) has a nearly quarter-century history of exposing corruption and encouraging transparency in government decision-making and public policy formation. Since its 1993 founding by former World Bank program manager Peter Eigen in Berlin, Germany, the organization has extended its reach through more than 100 worldwide chapters. Expansive in the sheer volume of information it promulgates, the website is a testament to the group’s larger mission to expose abuses of power and corruption on a global scale.

Best known are the regular report series (and corresponding site sections) Global Corruption Barometer, Global Corruption Report, Bribe Payers Index, and Corruption Perceptions Index—all of which serve to monitor and report on abuses or potentials for corruption. Also featured are interactive maps, aggregate indexes, risk assessments, and surveys. Research tools include country guides and topical guides covering corruption across the spectrum (in dozens of areas, from sports, forestry, or oil and gas industries, to the judiciary). Well-produced reports are available in downloadable PDF or HTML formats. Of particular interest for advanced scholars is the Gateway Corruption Assessment Toolbox, which provides over 500 diagnostic tools (complete with user guides) designed to measure and expose corruption. Other highlights are a set of Corruption Fighters’ Toolkits for activists and educators interested in furthering the global anti-corruption movement, or those offering watchdog groups specific practical guidance (e.g., Integrity Pacts, applicable to public procurement and bidding processes). An additional teaching tool is the Anti-Corruption Glossary, defining terms like “beneficial ownership secrecy,” “clientelism,” “illicit financial flows,” and “tax haven.” Of particular interest to librarians (under the What We Do tab) are an Access to Information section and the Anti-Corruption Helpdesk, offering on-demand research support.

Page links and social tags help users navigate throughout the site and serve to interrelate subjects, regions, countries, and languages. Despite its clean design, the site is so complex that locating specific offerings—like the Open Governance Project (“Helping Citizens Engage with Government”) in the Programmes, Projects, Activities section—might be more easily accomplished by doing a simple Google search. The site does an excellent job informing, educating, and inspiring, not only pointing out news, documentaries, blog, and *Twitter* posts, but also providing a platform to foster social networks. It remains a preeminent virtual space for activism and an excellent tool for research. **Summing Up:** Highly recommended. All readership levels. *—A. I. Fritz, University of Notre Dame*

----

Line2: 

----

Author: Choice Staff

----

Author-img: 

----

Byline: 

----

References: 