Title: April Hot Topic: Climate Change

----

Subtitle: Selected titles reviewed in Choice about climate change.

----

Category: Newsletters

----

Date: 2017-04-25

----

Description: Selected titles reviewed in Choice about climate change.

----

Line1: 

----

Thumb: ht-apr-2017.jpg

----

Hero: ht-apr-2017.jpg

----

Hero-caption: 

----

Text: 

Jamieson, Dale. **Reason in a dark time: why the struggle against climate change failed and what it means for our future**. Oxford, 2014. 266p bibl index afp ISBN 9780199337668, $29.95.
*Reviewed in CHOICE November 2014*

When this reviewer began reading *Reason in a Dark Time*, he was impressed by the thorough treatment of the political and social history of climate change.  However, as he continued reading, he began to understand that this is more a story of social ethics than it is a summation of the past 40 years of determining what to do about the effects that anthropogenic emissions have had and continue to have on global climate.  Jamieson (NYU; Ethics and the Environment, (link: http://www.choicereviews.org text: CH, Oct'08, 46-0811 popup: yes)) deftly introduces readers to the difficulties, triumphs, setbacks, and damning inaction that defines the climate debate.  He then carefully explains that the problem is much more than debates, hand-wringing, and conflicts of interests.  He describes a moral failure of the global community, scientists, politicians, and the general public.  His book leaves one feeling that democracy, which depends on an engaged and educated citizenry, is doomed to failure, at least by 21st-century standards.  Though there is some room for hope, Jamieson takes a pragmatic view and paints a rather dark picture.  Forget learning about the science of climate change; read this book to learn about society and perhaps do a little bit of soul-searching. **Summing Up:** Highly recommended. All levels/libraries. *—M. Schaab, Maine Maritime Academy*

***
Kiehl, Jeffrey T. **Facing climate change: an integrated path to the future**. Columbia, 2016. 159p bibl index afp ISBN 9780231177184, $28.00; ISBN 9780231541169 ebook, $27.99.
*Reviewed in CHOICE November 2016*

Kiehl, a senior scientist at the National Center for Atmospheric Research, draws on his uniquely paired expertise in climate science and Jungian psychology to present a fresh perspective on the climate crisis. This is a truly interdisciplinary book—it weaves together elements of science, psychology, philosophy, and Buddhism to explore the roots of ongoing climate denial and inaction as well as a path forward toward a sustainable future. Ultimately, Kiehl highlights a need for a greater sense of interconnectedness and increased compassion. This, he contends, can serve as the basis for overcoming destructive patterns of behavior and transforming human relationships with the natural world and one another. Keeping his writing accessible as he moves across diverse subject matter, Kiehl finds a successful balance between brevity and substance that enables him to meaningfully introduce key concepts and make insightful connections among disciplines. The book’s personable style and interdisciplinary approach will interest readers with focuses ranging from psychology and spirituality to climate change. **Summing Up: **Recommended. Upper-division undergraduates through researchers and faculty; professionals.* —J. L. Rhoades, Antioch University New England*

***

Mann, Michael E. **The madhouse effect: how climate change denial is threatening our planet, destroying our politics, and driving us crazy**, by Michael E. Mann and Tom Toles. Columbia, 2016. 186p index afp ISBN 9780231177863, $24.95; ISBN 9780231541817 ebook, $23.99.
*Reviewed in CHOICE April 2017 *

Mann (Penn State Univ.) is a well-known climatologist who specializes in the field of climate change. Toles is a Pulitzer Prize–winning political cartoonist with biting wit. The two have teamed up to produce a timely, slim (150 pages of text), illustrated volume that attacks those who deny facts and implications of anthropogenic climate change. Scientists have reached the consensus that humans are radically changing climate—the data and analyses have been thoroughly tested—and readers are presented with this information. The data and analyses are referenced in further detail so readers can methodically comprehend the issue. The potential for disastrous consequences (e.g., losing the world's coastal infrastructure, billions of refugees, conflict over increasingly scarce resources) is discussed. The work then explores US climate change denial, referencing the tobacco industry's “disinformation campaign” to delay public recognition of tobacco's health risks. The fossil fuel industry is mirroring this exact approach with climate change. The authors also contend that lavish contributions by the industry created a “Republican war on science”—essentially corrupting a political party. Delay in mitigating this threat (caused by Republican obstruction) may have catastrophic consequences. **Summing Up:** Recommended. All readers. *—M. K. Cleaveland, University of Arkansas at Fayetteville*

***

McGraw, Seamus. **Betting the farm on a drought: stories from the front lines of climate change**. Texas, 2015. 180p bibl index afp ISBN 9780292756618, $24.95.
*Reviewed in CHOICE January 2016*

Award-winning science journalist McGraw takes readers on a journey beyond polarized debate in order to provide a better understanding of how climate change is already impacting ordinary US citizens.  McGraw introduces readers to farmers, fishermen, and ranchers living at the front lines of climate change.  He uses their stories to show how climate change is affecting their lives and how they are struggling to find creative solutions.  He complements these stories with interviews of scientists and policy makers, discussion of recent climate science, and a history of the climate debate.  Most important, by connecting climate change with individual stories, the author presents a world in which lived experience and a need to persevere trump political affiliation and dogma.  In so doing, he offers hope for moving past the current intractable debate and coming together for reasoned conversation about pragmatic concerns.  Effectively blending story, science, and context, this engaging, readable book will be invaluable for those studying or working on issues associated with climate change, especially those with a social science or policy focus. **Summing Up:** Essential. Upper-division undergraduates through faculty and professionals; general readers. *—J. L. Rhoades, Antioch University New England*

***

Nuccitelli, Dana. **Climatology versus pseudoscience: exposing the failed predictions of global warming skeptics**. Praeger, 2015. 212p index afp ISBN 9781440832017, $48.00; ISBN 9781440832024 ebook, contact publisher for price.
*Reviewed in CHOICE October 2015*

The author is a physics-trained environmental scientist who describes his evolution from initial skepticism about Al Gore’s *An Inconvenient Truth* to a conviction that climate change is one of the greatest threats to the human race.  The book includes the best available short account of the history of climate science and describes climate change models and controversies in brief but incisive and well-illustrated detail.  The main focus is the baffling fact that, in spite of the “97 percent consensus in peer-reviewed climate science literature,” most Americans are unconcerned about climate change, are in denial, or rank global warming low among their priorities.  Nuccitelli contrasts the “astounding accuracy of early climate models” with inconsistencies and erroneous claims in statements by contrarian scientists, whose views, he argues, are supported more by ideology than by facts.  The “consensus gap” is explained in part by the way American and British media cover controversial subjects; for example, conservative media such as the *Wall Street Journal* overrepresent climate contrarians.  On the other side, unwittingly counterproductive debunkers of climate denial produce “the overkill backfire effect.”  Such mistakes by climate campaigners are emphasized in Don’t Even Think about It ((link: http://www.choicereviews.org text: CH, Apr'15, 52-4229 popup: yes)).  Nuccitelli's book is particularly valuable for its succinct treatment of key disputes without the heated rhetoric. **Summing Up:** Essential. Upper-division undergraduates through professionals and practitioners. *—F. T. Manheim, George Mason University*

***

Orr, David W. **Dangerous years: climate change, the long emergency, and the way forward**. Yale, 2016. 300p index afp ISBN 9780300222814, $28.50; ISBN 9780300225105 ebook, contact publisher for price.
*Reviewed in CHOICE May 2017*

Climate change is often thought of in terms of rising temperatures, more variable precipitation, and increasing carbon dioxide, with impacts on both the terrestrial and aquatic ecosystems. The prevailing thought is that reducing the carbon dioxide levels will eliminate or at least reduce the impacts of climate change. Orr (emer., Oberlin College) thoroughly argues that climate change is more complex than the physical manifestations of increasing carbon dioxide levels in the atmosphere. The impacts of climate change are a delicate balance of economics, sociology, political structures, and the willingness of the human race to understand the complexity of the problem and even more importantly, the complexity of the *solution*. In this interesting treatise, there is an underlying premise that solutions will not come through optimism but through facing the real facts that a solution may not be possible, and if so, the length of time for the effect will lead to a much different Earth than what exists today. There is constant discussion of what it takes to create climate resilience. The argument is that creating resilience will require a focus on a truly “wicked problem.” Are we willing to change to allow for viable solutions? **Summing Up:** Recommended. Lower-division undergraduates and above; faculty and professionals. *—J. L. Hatfield, USDA-Agricultural Research Service*

***

Peters, E. Kirsten. **The whole story of climate: what science reveals about the nature of endless change**. Prometheus Books, 2012. 290p ISBN 1616146729, $26.00; ISBN 9781616146726, $26.00.
*Reviewed in CHOICE May 2013*

This brilliant, engaging book will change the way one looks at the climate/global warming debate, climate change and climate science, human versus nonhuman drivers of climate change, and the implications of global warming/cooling for humans and societies. Geologist/journalist Peters (Washington State Univ.) is thoughtful, thorough, and relentless in her description of the breakthroughs and characters instrumental in finding evidence for dramatic climate swings from icehouse to hothouse conditions that have plagued Earth over the last 2.5 million years and the implications of these sometime rapid changes on humans and the development of societies and civilization. The author writes in a clear, almost storytelling style that brings to light geologic evidence rarely discussed in the present global warming debate. The evidence for a history of climate change is dispassionately yet engagingly presented. Peters does, however, take some license in chiding climate modelers for ignoring Earth's past climate history and bemoaning their present singular focus on atmospheric CO<sub>2</sub> and excluding geologists and the geological record from their discussions and reports. Given the author's passion for the subject and the outstanding nature of the book, this foible is easily forgiven. **Summing Up:** Essential. All levels/libraries. *—B. Ransom, University of California, San Diego*

***

Stone, Brian, Jr. **The city and the coming climate: climate change in the places we live**. Cambridge, 2012. 187p ISBN 9781107016712, $99.00; ISBN 9781107602588 pbk, $29.99.
*Reviewed in CHOICE November 2012*

This is a highly readable, informative book on many overlooked aspects of climate change that are perhaps more important to people and society than simply the steadily increasing concentration of CO<sub>2</sub> in the atmosphere considered in most models of climate change. When it comes to people, as Stone (city and regional planning, Georgia Institute of Technology) discusses in this well-written volume, the urban heat island effect and land use changes associated with increasing population pressure should be high on everyone's radar screens, but they are not. This book clearly presents information on the importance of these two underappreciated global warming components and demonstrates how they are dramatically changing the planet and its cities and their habitability. Particularly interesting are discussions of demonstrated techniques to decrease the smothering heat generated from cityscapes that can cause urban temperatures to be sometimes 10-15 degrees Fahrenheit warmer than surrounding undeveloped areas. Also of interest are discussions of how land use changes are dramatically, and negatively, changing precipitation patterns in both arid and tropical climates. This excellent volume does not simply reiterate what has been said many times before. Stone's clear writing enables anyone to understand and appreciate the importance of these local to regional climate factors. **Summing Up:** Highly recommended. All readership levels. *—B. Ransom, University of California, San Diego*

****

Woodworth, Paddy. **Our once and future planet: restoring the world in the climate change century**. Chicago, 2013. 515p bibl index afp ISBN 9780226907390, $35.00.
*Reviewed in CHOICE April 2014*

*Our Once and Future Planet* offers a unique survey of restoration case studies emphasizing process and outcome. Author/journalist Woodworth (formerly, Irish Times; Dirty Water, Clean Hands, (link: http://www.choicereviews.org text: CH, Jan'02, 39-3033 popup: yes)) has previously written about human conflicts in Ireland or in the Basque region. Here, he carefully unpacks the arguments--both ecological and social--in restoration efforts from Chicago to South Africa, Ireland to New Zealand, and Australia to Costa Rica, encompassing a suite of restoration goals. The author examines multiple dimensions of restoration, such as urban restoration, restoration to build social and economic resilience, restoration of cultural landscapes, and the use of chemicals to remove invasive animals and plants. All the while, Woodworth probes the questions of defining the restoration goal, meeting public expectations, monitoring outcomes, and using citizen workers. A much-needed resource for those interested in the implementation and sustenance of long-term restoration efforts, the book will stimulate discussion and reassessment of restoration in multiple contexts. Woodworth also brings the cases together under the umbrella of restoration theory in the penultimate chapter. This title is an excellent resource for those desiring a less technical, more accessible piece examining social, cultural, political, and economic interactions with the science of restoration. **Summing Up:** Highly recommended. Upper-division undergraduates and above; general readers. *—L. Broberg, University of Montana*

****

Zalasiewicz, Jan. **The Goldilocks planet: the four billion year story of Earth's climate**, by Jan Zalasiewicz and Mark Williams. Oxford, 2012. 303p ISBN 9780199593576, $29.95.
*Reviewed in CHOICE November 2012*

The climate change debate has long been dominated by climatologists, politicians, and economists, but the contributions of geologists to an understanding of this issue have been underreported. In *The Goldilocks Planet*, geologists Zalasiewicz and Williams (both, Univ. of Leicester, UK) synthesize a vast body of work on paleoenvironmental reconstruction and paleoclimate through geologic time. They identify the greenhouse and icehouse episodes from the Archaean eon to the present and explain how these conditions waxed and waned. The authors concentrate on the warming and cooling episodes from the Pliocene period (prior to the Pleistocene glaciations) to date and use substantial and diverse recent research findings. The Earth is now thought to be headed to that Pliocene warming benchmark. Zalasiewicz and Williams provide simple explanations of the astronomical, geological, chemical, and geographic factors that weave into the natural greenhouse and icehouse episodes. This scholarly book is well written and documented, and the authors make good use of analogies to convey the scale and importance of the processes at work. Along the way, readers also learn about the scientists in many fields who have contributed to the development of these ideas. **Summing Up:** Highly recommended. Upper-division undergraduates and above. *—L. S. Zipp, State University of New York College at Geneseo*

----

Line2: 

----

Author: Choice Staff

----

Author-img: 

----

Byline: 

----

References: 