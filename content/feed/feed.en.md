Title: Choice RSS Feed

----

Text: Choice offers a community where librarians come together to share knowledge and build stronger institutions. Our free development tools connect you to the latest sources, technologies, and practices influencing scholarly research and publishing.
