// load tasks
var gulp          = require('gulp'),
    sass          = require('gulp-sass'),
    autoprefixer  = require('gulp-autoprefixer'),
    cssnano       = require('gulp-cssnano'),
    uglify        = require('gulp-uglify'),
    imagemin      = require('gulp-imagemin'),
    rename        = require('gulp-rename'),
    concat        = require('gulp-concat'),
    notify        = require('gulp-notify'),
    cache         = require('gulp-cache'),
    browserSync   = require('browser-sync'),
    pixrem        = require('gulp-pixrem'),
    del           = require('del');


// config
var proxyUrl = 'localhost:8888/choice360.org/';


// css
gulp.task('styles', function() {
  return gulp.src('assets/scss/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 3 versions', '> 1%'],
      cascade: false
    }))
    .pipe(pixrem('20px'))
    .pipe(gulp.dest('assets/build/css'))
    .pipe(rename({suffix: '.min'}))
    .pipe(cssnano())
    .pipe(gulp.dest('assets/build/css'));
});


// js
gulp.task('scripts', function() {
  return gulp.src('assets/js/**/*.js')
    .pipe(concat('main.js'))
    .pipe(gulp.dest('assets/build/js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('assets/build/js'));
});


// flush the build folder to make sure the files are up to date
gulp.task('clean', function() {
  return del(['assets/build/css', 'assets/build/js']);
});


// default task
gulp.task('default', ['clean'], function() {

  // rebuild files after flushing
  gulp.start('styles', 'scripts');

  // Initiate browsersync server (requires mamp)
  browserSync.init({
    proxy: proxyUrl
  });

  // Watch .scss files
  gulp.watch(['assets/scss/**/*.scss', 'site/patterns/**/*.scss'], ['styles']).on('change', browserSync.reload);

  // Watch .js files
  gulp.watch('assets/js/**/*.js', ['scripts']).on('change', browserSync.reload);

  // watch content and template files
  gulp.watch(['site/**/*.php', 'content/**/*.txt', 'content/**/*.md', 'content/**/*.markdown']).on('change', browserSync.reload);

});
